package auto.avianca.common.web.enumtypes;

public enum PackageType {
	
	FLIGHTHOTEL,FLIGHTHOTELCAR,FLIGHTHOTELACTIVITY,NONE;
	
	public static PackageType getPackageType(String Pkg_Type){
		if(Pkg_Type.equalsIgnoreCase("Flight + Hotel")) return FLIGHTHOTEL;
		else if(Pkg_Type.equalsIgnoreCase("Flight + Hotel + Car")) return FLIGHTHOTELCAR;
		else if(Pkg_Type.equalsIgnoreCase("Flight + Hotel + Activities")) return FLIGHTHOTELACTIVITY;
		else return NONE;
	}

}
