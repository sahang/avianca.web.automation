package auto.avianca.common.web.enumtypes;

public enum DepartureType {
	
	ALL,MORNING,AFTERNOON,EVENING,NONE;
	
	public static DepartureType getDepartureType(String Depart_Type){
		if(Depart_Type.equalsIgnoreCase("ALL")) return ALL;
		else if(Depart_Type.equalsIgnoreCase("Morning")) return MORNING;
		else if(Depart_Type.equalsIgnoreCase("Afternoon")) return AFTERNOON;
		else if(Depart_Type.equalsIgnoreCase("Evening")) return EVENING;
		else return NONE;
	}

}
