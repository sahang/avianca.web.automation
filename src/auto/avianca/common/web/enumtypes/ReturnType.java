package auto.avianca.common.web.enumtypes;

public enum ReturnType {
	
	ALL,MORNING,AFTERNOON,EVENING,NONE;
	
	public static ReturnType getReturnType(String Ret_Type){
		if(Ret_Type.equalsIgnoreCase("ALL")) return ALL;
		else if(Ret_Type.equalsIgnoreCase("Morning")) return MORNING;
		else if(Ret_Type.equalsIgnoreCase("Afternoon")) return AFTERNOON;
		else if(Ret_Type.equalsIgnoreCase("Evening")) return EVENING;
		else return NONE;
	}

}
