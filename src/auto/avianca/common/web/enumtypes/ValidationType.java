package auto.avianca.common.web.enumtypes;

public enum ValidationType {
	RESERVATION,BOOKINGENGINE,FILTER,NONE;
	
	public static ValidationType getValidationType(String Seat_Class){
		if(Seat_Class.equalsIgnoreCase("Reservation")) return RESERVATION;
		else if(Seat_Class.equalsIgnoreCase("BookingEngine")) return BOOKINGENGINE;
		else if(Seat_Class.equalsIgnoreCase("Filter")) return FILTER;
		else return NONE;
	}

}
