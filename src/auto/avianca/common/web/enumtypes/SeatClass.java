package auto.avianca.common.web.enumtypes;

public enum SeatClass {
	ECONOMY,BUSINESS,NONE;
	
	public static SeatClass getSeatClass(String Seat_Class){
		if(Seat_Class.equalsIgnoreCase("Economy")) return ECONOMY;
		else if(Seat_Class.equalsIgnoreCase("Business")) return BUSINESS;
		else return NONE;
	}

}
