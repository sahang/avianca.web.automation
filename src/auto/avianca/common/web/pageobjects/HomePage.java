package auto.avianca.common.web.pageobjects;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import auto.avianca.common.web.utilities.PG_Properties;
//import auto.avianca.holidays.web.pageobjects.PropertyReader;
import auto.avianca.common.web.utilities.PhraseElement;;

public class HomePage {

	
	
	public HomePage() {
		
	}
	
	public static class BookingEngine{
		
		public static WebElement drop_languageBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("BookingEngine");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_languageBox")));
				logger.info("Booking engine dropdown languagebox available");
			} catch (Exception e) {
				logger.fatal("Booking engine dropdown languagebox not available ",e);
			}
			return element;
		}
		
		public static WebElement button_packageButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("BookingEngine");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_packageButton")));
				logger.info("Booking engine package button available");
			} catch (Exception e) {
				logger.fatal("Booking engine package button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_hotelButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("BookingEngine");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_hotelButton")));
				logger.info("Booking engine hotel button available");
			} catch (Exception e) {
				logger.fatal("Booking engine hotel button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_carButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("BookingEngine");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_carButton")));
				logger.info("Booking engine car button available");
			} catch (Exception e) {
				logger.fatal("Booking engine car button not available ",e);
			}
			return element;
		}
		
		public static WebElement dtp_DatePicker(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("BookingEngine");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_DatePicker")));
				logger.info("Booking engine datetimepicker available");
			} catch (Exception e) {
				logger.fatal("Booking engine date time picker not available ",e);
			}
			return element;
		}
	}
	
	
// Package Screen
	public static class PackageScreen{
		
		public static WebElement radio_flightHotelButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_flightHotelButton")));
				logger.info("Package screen Flight+Hotel button available");
				
			} catch (Exception e) {
				logger.fatal("Package screen Flight+Hotel button not available",e);
			}
			return element;
		}
		
		public static WebElement radio_flightHotelCarButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_flightHotelCarButton")));
				logger.info("Package screen Flight+Hotel+Car button available");
			} catch (Exception e) {
				logger.fatal("Package screen Flight+Hotel+Car button not available",e);
			}
			return element;
		}
		
		public static WebElement radio_flightHotelActivitiesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_flightHotelActivitiesButton")));
				logger.info("Package screen Flight+Hotel+Activities button available");
			} catch (Exception e) {
				logger.fatal("Package screen Flight+Hotel+Activities button not available",e);
			}
			return element;
		}
		
		public static WebElement text_fromField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_fromField")));
				logger.info("Package screen From text field available");
			} catch (Exception e) {
				logger.fatal("Package screen From text field  not available",e);
			}
			return element;
		}
		
		public static WebElement text_toField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_toField")));
				logger.info("Package screen To text field available");
			} catch (Exception e) {
				logger.fatal("Package screen To text field not available",e);
			}
			return element;
		}
		
		public static WebElement button_allCitiesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_allCitiesButton")));
				logger.info("Package screen All cities link button available");
			} catch (Exception e) {
				logger.fatal("Package screen All cities link button not available",e);
			}
			return element;
		}
		
		public static WebElement button_allColombianCitiesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_allColombianCitiesButton")));
				logger.info("Package screen All colombian cities available");
			} catch (Exception e) {
				logger.fatal("Package screen All colombian cities not available",e);
			}
			return element;
		}
		
		public static WebElement button_allDestinationsButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_allDestinationsButton")));
				logger.info("Package screen All destinations button available");
			} catch (Exception e) {
				logger.fatal("Package screen All destinations button not available",e);
			}
			return element;
		}
		
		public static WebElement button_allDestinationCitiesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_allDestinationCitiesButton")));
				logger.info("Package screen All destination cities available");
			} catch (Exception e) {
				logger.fatal("Package screen All destination cities not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_departDate")));
				logger.info("Package Departure date dtp available");
			} catch (Exception e) {
				logger.fatal("Package Departure date dtp not available",e);
			}
			return element;
		}
		
		public static WebElement drop_departTimeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_departTimeBox")));
				logger.info("Package screen Departure time drop down available");
			} catch (Exception e) {
				logger.fatal("Package screen Departure time drop down not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_returnDate")));
				logger.info("Package screen Return date dtp available");
			} catch (Exception e) {
				logger.fatal("Package screen Return date dtp not available",e);
			}
			return element;
		}
		
		public static WebElement drop_returnTimeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_returnTimeBox")));
				logger.info("Package screen Return time dropdown available");
			} catch (Exception e) {
				logger.fatal("Package screen Return time dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfRoomsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfRoomsBox")));
				logger.info("Package screen Number of rooms dropdown available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of rooms dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfAdultsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfAdultsBox")));
				logger.info("Package screen Number of adults dropdown available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of adults dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfChildrenBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfChildrenBox")));
				logger.info("Package screen Number of children dropdown available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of children dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfInfantsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfInfantsBox")));
				logger.info("Package screen Number of infants dropdown available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of infants dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement drop_seatClassBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_seatClassBox")));
				logger.info("Package screen Seat class dropdown available");
			} catch (Exception e) {
				logger.fatal("Package screen Seat class dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement text_PromotionCode(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_PromotionCode")));
				logger.info("Package screen Promotion code textfield available");
			} catch (Exception e) {
				logger.fatal("Package screen Promotion code textfield not available",e);
			}
			return element;
		}
		
		public static WebElement check_directFirstBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_directFirstBox")));
				logger.info("Package screen Display direct flight first checkbox available");
			} catch (Exception e) {
				logger.fatal("Package screen Display direct flight first checkbox not available",e);
			}
			return element;
		}
		
		public static WebElement check_partialStayBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_partialStayBox")));
				logger.info("Package screen Partial stay in hotel checkbox available");
			} catch (Exception e) {
				logger.fatal("Package screen Partial stay in hotel checkbox not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_partialSatyCheckInDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_partialSatyCheckInDate")));
				logger.info("Package screen Partial stay in hotel check In date DTP available");
			} catch (Exception e) {
				logger.fatal("Package screen Partial stay in hotel check In date DTP not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_partialSatyCheckOutDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_partialSatyCheckOutDate")));
				logger.info("Package screen Partial stay in hotel check Out date DTP available");
			} catch (Exception e) {
				logger.fatal("Package screen Partial stay in hotel check Out date DTP not available",e);
			}
			return element;
		}
		
		public static WebElement check_hotelOtherCityBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_hotelOtherCityBox")));
				logger.info("Package screen Hotel in other city checkbox available");
			} catch (Exception e) {
				logger.fatal("Package screen Hotel in other city checkbox not available",e);
			}
			return element;
		}
		
		public static WebElement text_partialStayCityField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_partialStayCityField")));
				logger.info("Package screen partial stay hotel city checkbox available");
			} catch (Exception e) {
				logger.fatal("Package screen partial stay hotel city checkbox not available",e);
			}
			return element;
		}
		
		public static WebElement button_searchButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_searchButton")));
				logger.info("Package screen Search button available");
			} catch (Exception e) {
				logger.fatal("Package screen Search button not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightHotel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightHotel")));
				logger.info("Package screen Flight+Hotel label available");
			} catch (Exception e) {
				logger.fatal("Package screen Flight+Hotel label not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightHotelCar(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightHotelCar")));
				logger.info("Package screen Flight+Hotel+Car label available");
			} catch (Exception e) {
				logger.fatal("Package screen Flight+Hotel+Car label not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightHotelActivities(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightHotelActivities")));
				logger.info("Package screen Flight+Hotel+Activities label available");
			} catch (Exception e) {
				logger.fatal("Package screen Flight+Hotel+Activities label not available",e);
			}
			return element;
		}
		
		public static WebElement label_from(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_from")));
				logger.info("Package screen From label available");
			} catch (Exception e) {
				logger.fatal("Package screen From label not available",e);
			}
			return element;
		}
		
		public static WebElement label_to(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_to")));
				logger.info("Package screen To label available");
			} catch (Exception e) {
				logger.fatal("Package screen To label not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDate")));
				logger.info("Package screen Depart date label available");
			} catch (Exception e) {
				logger.fatal("Package screen Depart date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_departTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departTime")));
				logger.info("Package screen Depart time label available");
			} catch (Exception e) {
				logger.fatal("Package screen Depart time label not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDate")));
				logger.info("Package screen Return date label available");
			} catch (Exception e) {
				logger.fatal("Package screen Return date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnTime")));
				logger.info("Package screen Return time label available");
			} catch (Exception e) {
				logger.fatal("Package screen Return time label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfRooms(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfRooms")));
				logger.info("Package screen Number of rooms label available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of rooms label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfAdults(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfAdults")));
				logger.info("Package screen Number of adults label available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of adults label not available",e);
			}
			return element;
		}
		
		public static WebElement label_adultAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adultAge")));
				logger.info("Package screen Adult age label available");
			} catch (Exception e) {
				logger.fatal("Package screen Adult age label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfChildren(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfChildren")));
				logger.info("Package screen Number of children label available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of children label not available",e);
			}
			return element;
		}
		
		public static WebElement label_childrenAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_childrenAge")));
				logger.info("Package screen Children age label available");
			} catch (Exception e) {
				logger.fatal("Package screen Children age label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfInfants(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfInfants")));
				logger.info("Package screen Number of infants label available");
			} catch (Exception e) {
				logger.fatal("Package screen Number of infants label not available",e);
			}
			return element;
		}
		
		public static WebElement label_infantAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_infantAge")));
				logger.info("Package screen Infant age label available");
			} catch (Exception e) {
				logger.fatal("Package screen Infant age label not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomNo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomNo")));
				logger.info("Package screen Room number label available");
			} catch (Exception e) {
				logger.fatal("Package screen Room number label not available",e);
			}
			return element;
		}
		
		public static WebElement label_seatClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_seatClass")));
				logger.info("Package screen Seat class label available");
			} catch (Exception e) {
				logger.fatal("Package screen Seat class label not available",e);
			}
			return element;
		}
		
		public static WebElement label_promotionCode(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_promotionCode")));
				logger.info("Package screen Promotion code label available");
			} catch (Exception e) {
				logger.fatal("Package screen Promotion code label not available",e);
			}
			return element;
		}
		
		public static WebElement label_directFirst(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_directFirst")));
				logger.info("Package screen First display direct flight label available");
			} catch (Exception e) {
				logger.fatal("Package screen First display direct flight label not available",e);
			}
			return element;
		}
		
		public static WebElement label_partialStay(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_partialStay")));
				logger.info("Package screen Partial stay in the hotel label available");
			} catch (Exception e) {
				logger.fatal("Package screen Partial stay in the hotel label not available",e);
			}
			return element;
		}
		
		public static WebElement label_partialStayCheckInDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_partialStayCheckInDate")));
				logger.info("Package screen Partial stay check in date label available");
			} catch (Exception e) {
				logger.fatal("Package screen Partial stay check in date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_partialStayCheckOutDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_partialStayCheckOutDate")));
				logger.info("Package screen Partial stay check out date label available");
			} catch (Exception e) {
				logger.fatal("Package screen Partial stay check out date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelOtherCity(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelOtherCity")));
				logger.info("Package screen Hotel in other city label available");
			} catch (Exception e) {
				logger.fatal("Package screen Hotel in other city label not available",e);
			}
			return element;
		}
		
		public static WebElement label_partialStayCity(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_partialStayCity")));
				logger.info("Package partial stay hotel city label available");
			} catch (Exception e) {
				logger.fatal("Package partial stay hotel city label not available",e);
			}
			return element;
		}
	}
	
// Hotel Screen
	public static class HotelScreen{
		
		public static WebElement text_locationField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_locationField")));
				logger.info("Hotel screen Location textfield available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Location textfield not available",e);
			}
			return element;
		}
		
		public static WebElement button_allDestinationsButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_allDestinationsButton")));
				logger.info("Hotel screen All destinations button available");
			} catch (Exception e) {
				logger.fatal("Hotel screen All destinations button not available",e);
			}
			return element;
		}
		
		public static WebElement button_allDestinations2Button(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_allDestinations2Button")));
				logger.info("Hotel screen All destinations list available");
			} catch (Exception e) {
				logger.fatal("Hotel screen All destinations list not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_checkInDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_checkInDate")));
				logger.info("Hotel screen Check in date dtp available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Check in date dtp not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_checkOutDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_checkOutDate")));
				logger.info("Hotel screen Check out date dtp available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Check out date dtp not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfRoomsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfRoomsBox")));
				logger.info("Hotel screen Number of rooms drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Number of rooms drop down not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfAdultsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfAdultsBox")));
				logger.info("Hotel screen Number of adults drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Number of adults drop down not available",e);
			}
			return element;
		}
		
		public static WebElement drop_noOfChildrenBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_noOfChildrenBox")));
				logger.info("Hotel screen Number of children drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Number of children drop down not available",e);
			}
			return element;
		}
		
		public static WebElement text_hotelNameField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_hotelNameField")));
				logger.info("Hotel screen Hotel name textfield available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Hotel name textfield not available",e);
			}
			return element;
		}
		
		public static WebElement drop_starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_starRatingBox")));
				logger.info("Hotel screen Star rating drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Star rating drop down not available",e);
			}
			return element;
		}
		
		public static WebElement button_searchButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_searchButton")));
				logger.info("Hotel screen Search button available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Search button not available",e);
			}
			return element;
		}
		
		public static WebElement label_location(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_location")));
				logger.info("Hotel screen Location label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Location label not available",e);
			}
			return element;
		}
		
		public static WebElement label_checkInDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_checkInDate")));
				logger.info("Hotel screen Check in date label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Check in date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_checkOutDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_checkOutDate")));
				logger.info("Hotel screen Check out date label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Check out date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfRooms(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfRooms")));
				logger.info("Hotel screen Number of rooms label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Number of rooms label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfAdults(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfAdults")));
				logger.info("Hotel screen Number of adults label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Number of adults label not available",e);
			}
			return element;
		}
		
		public static WebElement label_adultAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adultAge")));
				logger.info("Hotel screen Adult age label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Adult age label not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfChildren(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfChildren")));
				logger.info("Hotel screen Number of children label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Number of children label not available",e);
			}
			return element;
		}
		
		public static WebElement label_childAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_childAge")));
				logger.info("Hotel screen Child age label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Child age label not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomNo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomNo")));
				logger.info("Hotel screen Room number label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Room number label not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelName")));
				logger.info("Hotel screen Hotel name label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Hotel name label not available",e);
			}
			return element;
		}
		
		public static WebElement label_starRating(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_starRating")));
				logger.info("Hotel screen Star rating label available");
			} catch (Exception e) {
				logger.fatal("Hotel screen Star rating label not available",e);
			}
			return element;
		}
	}
	
//------------------------------------Car Screen--------------------------------------------
	public static class CarScreen{
		
		public static WebElement text_pickupLocationField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_pickupLocationField")));
				logger.info("Car screen Pickup location textfield available");
			} catch (Exception e) {
				logger.fatal("Car screen Pickup location textfield not available",e);
			}
			return element;
		}
		
		public static WebElement text_returnLocationField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_returnLocationField")));
				logger.info("Car screen Return location textfield available");
			} catch (Exception e) {
				logger.fatal("Car screen Return location textfield not available",e);
			}
			return element;
		}
		
		public static WebElement check_returnToPickupLocationBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_returnToPickupLocationBox")));
				logger.info("Car screen Return to pickup location checkbox available");
			} catch (Exception e) {
				logger.fatal("Car screen Return to pickup location checkbox not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_pickupDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_pickupDate")));
				logger.info("Car screen Pickup date dtp available");
			} catch (Exception e) {
				logger.fatal("Car screen Pickup date dtp not available",e);
			}
			return element;
		}
		
		public static WebElement drop_pickupTimeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_pickupTimeBox")));
				logger.info("Car screen Pickup time dropdown available");
			} catch (Exception e) {
				logger.fatal("Car screen Pickup time dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement dtp_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_returnDate")));
				logger.info("Car screen Return date dtp available");
			} catch (Exception e) {
				logger.fatal("Car screen Return date dtp not available",e);
			}
			return element;
		}
		
		public static WebElement drop_returnTimeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_returnTimeBox")));
				logger.info("Car screen Return time dropdown available");
			} catch (Exception e) {
				logger.fatal("Car screen Return time dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement drop_carTypeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_carTypeBox")));
				logger.info("Car screen Car type dropdown available");
			} catch (Exception e) {
				logger.fatal("Car screen Car type dropdown not available",e);
			}
			return element;
		}
		
		public static WebElement button_searchButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_searchButton")));
				logger.info("Car screen Search button available");
			} catch (Exception e) {
				logger.fatal("Car screen Search button not available",e);
			}
			return element;
		}
		
		public static WebElement label_pickupLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_pickupLocation")));
				logger.info("Car screen Pickup location label available");
			} catch (Exception e) {
				logger.fatal("Car screen Pickup location label not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnLocation")));
				logger.info("Car screen Return location label available");
			} catch (Exception e) {
				logger.fatal("Car screen Return location label not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnToPickupLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnToPickupLocation")));
				logger.info("Car screen Return to pickup location label available");
			} catch (Exception e) {
				logger.fatal("Car screen Return to pickup location label not available",e);
			}
			return element;
		}
		
		public static WebElement label_pickupDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_pickupDate")));
				logger.info("Car screen Pickup date label available");
			} catch (Exception e) {
				logger.fatal("Car screen Pickup date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_pickupTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_pickupTime")));
				logger.info("Car screen Pickup time label available");
			} catch (Exception e) {
				logger.fatal("Car screen Pickup time label not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDate")));
				logger.info("Car screen Return date label available");
			} catch (Exception e) {
				logger.fatal("Car screen Return date label not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnTime")));
				logger.info("Car screen Return time label available");
			} catch (Exception e) {
				logger.fatal("Car screen Return time label not available",e);
			}
			return element;
		}
		
		public static WebElement label_carType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("CarScreen");
			
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_carType")));
				logger.info("Car screen Car type label available");
			} catch (Exception e) {
				logger.fatal("Car screen Car type label not available",e);
			}
			return element;
		}
		
	
	}
}
