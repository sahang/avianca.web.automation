package auto.avianca.common.web.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import auto.avianca.common.web.utilities.PG_Properties;
import auto.avianca.common.web.utilities.PhraseElement;

public class ResultsPage {
	
	public ResultsPage() {
		
	}
	
//Flight Results Page
	public static class FlightResultsPage{
		
		public static WebElement label_Country(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_Country")));
				logger.info("Flight Results page Label Country available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Label Country not available ",e);
			}
			return element;
		}
		
		public static WebElement label_Language(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_Language")));
				logger.info("Flight Results page Label Language available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Label Language not available ",e);
			}
			return element;
		}
		
		public static WebElement img_aviancaLogo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_aviancaLogo")));
				logger.info("Flight Results page Avianca Logo available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Avianca Logo not available ",e);
			}
			return element;
		}
		
		public static WebElement button_homeImageButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_homeImageButton")));
				logger.info("Flight Results page Home image button available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Home image button not available ",e);
			}
			return element;
		}
		
		public static WebElement img_searchPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_searchPanel")));
				logger.info("Flight Results page image search panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image search panle not available ",e);
			}
			return element;
		}
		
		public static WebElement img_selectionPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectionPanel")));
				logger.info("Flight Results page image selection panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image selection panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_reviewPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_reviewPanel")));
				logger.info("Flight Results page image review panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image review panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_confirmPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_confirmPanel")));
				logger.info("Flight Results page image confirm panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image confirm panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_selectFlightPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectFlightPanel")));
				logger.info("Flight Results page image select flight panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image select flight panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_selectHotelPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectHotelPanel")));
				logger.info("Flight Results page image select hotel panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image select hotel panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_selectCarPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectCarPanel")));
				logger.info("Flight Results page image select car panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image select car panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_selectActivityPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectActivityPanel")));
				logger.info("Flight Results page image select activity panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image select activity panel not available ",e);
			}
			return element;
		}
		
		public static WebElement img_summaryPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_summaryPanel")));
				logger.info("Flight Results page image summary panel available");
			} catch (Exception e) {
				logger.fatal("Flight Results page image summary panel not available ",e);
			}
			return element;
		}
		
		public static WebElement button_newSearchButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_newSearchButton")));
				logger.info("Flight Results page new search button available");
			} catch (Exception e) {
				logger.fatal("Flight Results page new search button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_depart(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_depart")));
				logger.info("Flight Results page Label depart available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Label depart not available ",e);
			}
			return element;
		}
		
		public static WebElement label_return(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_return")));
				logger.info("Flight Results page Label return available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Label return not available ",e);
			}
			return element;
		}
		
		public static WebElement label_departDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDetails")));
				logger.info("Flight Results page Label depart details available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Label depart details not available ",e);
			}
			return element;
		}
		
		public static WebElement label_returnDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDetails")));
				logger.info("Flight Results page Label return details available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Label return details not available ",e);
			}
			return element;
		}
		
		public static WebElement button_continueButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("FlightResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_continueButton")));
				logger.info("Flight Results page Continue button available");
			} catch (Exception e) {
				logger.fatal("Flight Results page Continue button not available ",e);
			}
			return element;
		}
	}
	
	
//Hotel Results Page
	public static class HotelResultsPage{
		
		public static WebElement img_aviancaLogo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_aviancaLogo")));
				logger.info("Hotel Results page avianca logo available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page avianca logo not available ",e);
			}
			return element;
		}
		
		public static WebElement button_homeImageButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_homeImageButton")));
				logger.info("Hotel Results page home page button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page home page button not available ",e);
			}
			return element;
		}
		
		public static WebElement img_searchPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_searchPanel")));
				logger.info("Hotel Results page search panel image available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page search panel image not available ",e);
			}
			return element;
		}
		
		public static WebElement img_selectionPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectionPanel")));
				logger.info("Hotel Results page selection panel image available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page selection panel image not available ",e);
			}
			return element;
		}
		
		public static WebElement img_reviewPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_reviewPanel")));
				logger.info("Hotel Results page review panel image available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page review panel image not available ",e);
			}
			return element;
		}
		
		public static WebElement img_confirmPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_confirmPanel")));
				logger.info("Hotel Results page confirm panel image available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page confirm panel image not available ",e);
			}
			return element;
		}
		
	//###### Change flight #######
		public static WebElement button_changeFlightButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_changeFlightButton")));
				logger.info("Hotel Results page change flight button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page change flight button not available ",e);
			}
			return element;
		}
		
		public static WebElement link_newPackageSearchButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_newPackageSearchButton")));
				logger.info("Hotel Results page new package search link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page new package search link button not available ",e);
			}
			return element;
		}
		
		public static WebElement link_viewHotelsOnMapButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_viewHotelsOnMapButton")));
				logger.info("Hotel Results page view hotels on map link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page view hotels on map link button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_sortBy(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_sortBy")));
				logger.info("Hotel Results page sort by label available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page sort by label not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_priceBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_priceBox")));
				logger.info("Hotel Results page price drop down box available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page price drop down box not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_starBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_starBox")));
				logger.info("Hotel Results page star drop down box available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page star drop down box not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_nameBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_nameBox")));
				logger.info("Hotel Results page name drop down box available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page name drop down box not available ",e);
			}
			return element;
		}
		
		public static WebElement button_modifyHotelDatesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_modifyHotelDatesButton")));
				logger.info("Hotel Results page modify hotel dates button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page modify hotel dates button not available ",e);
			}
			return element;
		}
		
		public static WebElement Label_toCity(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("Label_toCity")));
				logger.info("Hotel Results page label To city available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label To city not available ",e);
			}
			return element;
		}
		
		public static WebElement text_toCityField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_toCityField")));
				logger.info("Hotel Results page text To city available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page text To city not available ",e);
			}
			return element;
		}
		
		public static WebElement label_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDate")));
				logger.info("Hotel Results page label departure date available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label departure date not available ",e);
			}
			return element;
		}
		
		public static WebElement dtp_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_departDate")));
				logger.info("Hotel Results page dtp departure date available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page dtp departure date not available ",e);
			}
			return element;
		}
		
		public static WebElement label_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDate")));
				logger.info("Hotel Results page label return date available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label return date not available ",e);
			}
			return element;
		}
		
		public static WebElement dtp_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("dtp_returnDate")));
				logger.info("Hotel Results page dtp return date available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page dtp return date not available ",e);
			}
			return element;
		}
		
		public static WebElement label_roomCount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomCount")));
				logger.info("Hotel Results page label room count available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label room count not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_roomCountBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_roomCountBox")));
				logger.info("Hotel Results page room count drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page room count drop down not available ",e);
			}
			return element;
		}
		
		public static WebElement label_adults(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adults")));
				logger.info("Hotel Results page label adult count available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label adult count not available ",e);
			}
			return element;
		}
		
		public static WebElement label_adultAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adultAge")));
				logger.info("Hotel Results page label adult age available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label adult age not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_adultCountBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_adultCountBox")));
				logger.info("Hotel Results page adult count drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page adult count drop down not available ",e);
			}
			return element;
		}
		
		public static WebElement label_children(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_children")));
				logger.info("Hotel Results page label child count available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label child count not available ",e);
			}
			return element;
		}
		
		public static WebElement label_childAge(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_childAge")));
				logger.info("Hotel Results page label child age available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label child age not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_childCountBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_childCountBox")));
				logger.info("Hotel Results page child count drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page child count drop down not available ",e);
			}
			return element;
		}
		
		public static WebElement label_roomNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomNumber")));
				logger.info("Hotel Results page label room number available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label room number not available ",e);
			}
			return element;
		}
		
		public static WebElement label_hotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelName")));
				logger.info("Hotel Results page label hotel name available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label hotel name not available ",e);
			}
			return element;
		}
		
		public static WebElement text_hotelNameField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_hotelNameField")));
				logger.info("Hotel Results page hotel name text filed available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page hotel name text filed not available ",e);
			}
			return element;
		}
		
		public static WebElement label_starRating(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_starRating")));
				logger.info("Hotel Results page label start rating available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label start rating not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_starRatingBox")));
				logger.info("Hotel Results page start rating drop down available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page start rating drop down not available ",e);
			}
			return element;
		}
		
		public static WebElement button_search(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_search")));
				logger.info("Hotel Results page modify hotel dates search button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page modify hotel dates search button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_hotelFilters(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelFilters")));
				logger.info("Hotel Results page label hotel filters available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label hotel filters not available ",e);
			}
			return element;
		}
		
		public static WebElement button_resetFiltersButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_resetFiltersButton")));
				logger.info("Hotel Results page reset hotel filters button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page reset hotel filters button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_nameOfHotel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_nameOfHotel")));
				logger.info("Hotel Results page label name of hotel available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label name of hotel not available ",e);
			}
			return element;
		}
		
		public static WebElement text_nameOfHotelField(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_nameOfHotelField")));
				logger.info("Hotel Results page name of hotel text field available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page name of hotel text field not available ",e);
			}
			return element;
		}
		
		public static WebElement label_priceRange(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceRange")));
				logger.info("Hotel Results page label price range available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label price range not available ",e);
			}
			return element;
		}
		
		public static WebElement label_priceRangeFromTo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceRangeFromTo")));
				logger.info("Hotel Results page label price range from/to available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label price range from/to not available ",e);
			}
			return element;
		}
		
		public static WebElement priceRangeFilter(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("priceRangeFilter")));
				logger.info("Hotel Results page price range filter available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page price range filter not available ",e);
			}
			return element;
		}
		
		public static WebElement label_byStarRating(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_byStarRating")));
				logger.info("Hotel Results page label By star rating available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label By star rating not available ",e);
			}
			return element;
		}
		
		public static WebElement check_3starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_3starRatingBox")));
				logger.info("Hotel Results page 3 star rating checkbox available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page 3 star rating checkbox not available ",e);
			}
			return element;
		}
		
		public static WebElement check_4starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_4starRatingBox")));
				logger.info("Hotel Results page 4 star rating checkbox available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page 4 star rating checkbox not available ",e);
			}
			return element;
		}
		
		public static WebElement check_5starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_5starRatingBox")));
				logger.info("Hotel Results page 5 star rating checkbox available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page 5 star rating checkbox not available ",e);
			}
			return element;
		}
		
		public static WebElement check_6starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_6starRatingBox")));
				logger.info("Hotel Results page 6 star rating checkbox available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page 6 star rating checkbox not available ",e);
			}
			return element;
		}
		
		public static WebElement check_7starRatingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_7starRatingBox")));
				logger.info("Hotel Results page 7 star rating checkbox available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page 7 star rating checkbox not available ",e);
			}
			return element;
		}
		
		public static WebElement label_suburb(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_suburb")));
				logger.info("Hotel Results page label suburb available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label suburb not available ",e);
			}
			return element;
		}
		
		public static WebElement text_suburbBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_suburbBox")));
				logger.info("Hotel Results page textbox suburb available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page textbox suburb not available ",e);
			}
			return element;
		}
		
		public static WebElement label_nearByAttaractions(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_nearByAttaractions")));
				logger.info("Hotel Results page label near by attractions available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label near by attractions not available ",e);
			}
			return element;
		}
		
		public static WebElement drop_nearByAttaractionsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_nearByAttaractionsBox")));
				logger.info("Hotel Results page dropdown near by attractions available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page dropdown near by attractions not available ",e);
			}
			return element;
		}
		
		public static WebElement label_amenities(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_amenities")));
				logger.info("Hotel Results page label amenities available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label amenities not available ",e);
			}
			return element;
		}
		
		public static WebElement check_24HHelpDeskBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_24HHelpDeskBox")));
				logger.info("Hotel Results page checkbox 24 hour help desk available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox 24 hour help desk not available ",e);
			}
			return element;
		}
		
		public static WebElement label_24HHelpDesk(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_24HHelpDesk")));
				logger.info("Hotel Results page label 24 hour help desk available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label 24 hour help desk not available ",e);
			}
			return element;
		}
		
		public static WebElement check_airportTransferBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_airportTransferBox")));
				logger.info("Hotel Results page checkbox airport transfer available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox airport transfer not available ",e);
			}
			return element;
		}
		
		public static WebElement label_airportTransfer(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_airportTransfer")));
				logger.info("Hotel Results page label airport transfer available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label airport transfer not available ",e);
			}
			return element;
		}
		
		public static WebElement check_businessCenterBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_businessCenterBox")));
				logger.info("Hotel Results page checkbox business center available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox business center not available ",e);
			}
			return element;
		}
		
		public static WebElement label_businessCenter(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_businessCenter")));
				logger.info("Hotel Results page label business center available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label business center not available ",e);
			}
			return element;
		}
		
		public static WebElement check_directDialTelephoneBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_directDialTelephoneBox")));
				logger.info("Hotel Results page checkbox direct dial telephone available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox direct dial telephone not available ",e);
			}
			return element;
		}
		
		public static WebElement label_directDialTelephoneBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_directDialTelephoneBox")));
				logger.info("Hotel Results page label direct dial telephone available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label direct dial telephone not available ",e);
			}
			return element;
		}
		
		public static WebElement check_gymBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_gymBox")));
				logger.info("Hotel Results page checkbox gym available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox gym not available ",e);
			}
			return element;
		}
		
		public static WebElement label_gym(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_gym")));
				logger.info("Hotel Results page label gym available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label gym not available ",e);
			}
			return element;
		}
		
		public static WebElement check_internetAccessBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_internetAccessBox")));
				logger.info("Hotel Results page checkbox internet access available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox internet access not available ",e);
			}
			return element;
		}
		
		public static WebElement label_internetAccess(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_internetAccess")));
				logger.info("Hotel Results page label internet access available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label internet access not available ",e);
			}
			return element;
		}
		
		public static WebElement check_kitchenBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_kitchenBox")));
				logger.info("Hotel Results page checkbox kitchen available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox kitchen not available ",e);
			}
			return element;
		}
		
		public static WebElement label_kitchen(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_kitchen")));
				logger.info("Hotel Results page label kitchen available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label kitchen not available ",e);
			}
			return element;
		}
		
		public static WebElement check_laundryServiceBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_laundryServiceBox")));
				logger.info("Hotel Results page checkbox laundry service available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox laundry service not available ",e);
			}
			return element;
		}
		
		public static WebElement label_laundryService(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_laundryService")));
				logger.info("Hotel Results page label laundry service available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label laundry service not available ",e);
			}
			return element;
		}
		
		public static WebElement check_nightClubBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_nightClubBox")));
				logger.info("Hotel Results page checkbox night club available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox night club not available ",e);
			}
			return element;
		}
		
		public static WebElement label_nightClub(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_nightClub")));
				logger.info("Hotel Results page label night club available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label night club not available ",e);
			}
			return element;
		}
		
		public static WebElement check_nonSmokingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_nonSmokingBox")));
				logger.info("Hotel Results page checkbox non smoking available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox non smoking not available ",e);
			}
			return element;
		}
		
		public static WebElement label_nonSmoking(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_nonSmoking")));
				logger.info("Hotel Results page label non smoking available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label non smoking not available ",e);
			}
			return element;
		}
		
		public static WebElement check_parkingBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_parkingBox")));
				logger.info("Hotel Results page checkbox parking available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox parking not available ",e);
			}
			return element;
		}
		
		public static WebElement label_parking(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_parking")));
				logger.info("Hotel Results page label parking available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label parking not available ",e);
			}
			return element;
		}
		
		public static WebElement check_petFriendlyBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_petFriendlyBox")));
				logger.info("Hotel Results page checkbox pet friendly available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox pet friendly not available ",e);
			}
			return element;
		}
		
		public static WebElement label_petFriendly(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_petFriendly")));
				logger.info("Hotel Results page label pet friendly available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label pet friendly not available ",e);
			}
			return element;
		}
		
		public static WebElement check_restaurantBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_restaurantBox")));
				logger.info("Hotel Results page checkbox restaurant available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox restaurant not available ",e);
			}
			return element;
		}
		
		public static WebElement label_restaurant(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_restaurant")));
				logger.info("Hotel Results page label restaurant available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label restaurant not available ",e);
			}
			return element;
		}
		
		public static WebElement check_satelliteTelevisionBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_satelliteTelevisionBox")));
				logger.info("Hotel Results page checkbox satellite television available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox satellite television not available ",e);
			}
			return element;
		}
		
		public static WebElement label_satelliteTelevision(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_satelliteTelevision")));
				logger.info("Hotel Results page label satellite television available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label satellite television not available ",e);
			}
			return element;
		}
		
		public static WebElement check_smokingAreaBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_smokingAreaBox")));
				logger.info("Hotel Results page checkbox smoking area available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox smoking area not available ",e);
			}
			return element;
		}
		
		public static WebElement label_smokingArea(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_smokingArea")));
				logger.info("Hotel Results page label smoking area available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label smoking area not available ",e);
			}
			return element;
		}
		
		public static WebElement check_spaBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_spaBox")));
				logger.info("Hotel Results page checkbox spa available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox spa not available ",e);
			}
			return element;
		}
		
		public static WebElement label_spa(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_spa")));
				logger.info("Hotel Results page label spa available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label spa not available ",e);
			}
			return element;
		}
		
		public static WebElement check_swimmingPoolBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_swimmingPoolBox")));
				logger.info("Hotel Results page checkbox swimming pool available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox swimming pool not available ",e);
			}
			return element;
		}
		
		public static WebElement label_swimmingPool(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_swimmingPool")));
				logger.info("Hotel Results page label swimming pool available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label swimming pool not available ",e);
			}
			return element;
		}
		
		public static WebElement check_televisionBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_televisionBox")));
				logger.info("Hotel Results page checkbox television available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox television not available ",e);
			}
			return element;
		}
		
		public static WebElement label_television(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_television")));
				logger.info("Hotel Results page label television available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label television not available ",e);
			}
			return element;
		}
		
		public static WebElement check_wheelChairBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_wheelChairBox")));
				logger.info("Hotel Results page checkbox wheel chair available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox wheel chair not available ",e);
			}
			return element;
		}
		
		public static WebElement label_wheelChair(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_wheelChair")));
				logger.info("Hotel Results page label wheel chair available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label wheel chair not available ",e);
			}
			return element;
		}
		
		public static WebElement label_Show(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_Show")));
				logger.info("Hotel Results page label show available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label show not available ",e);
			}
			return element;
		}
		
		public static WebElement check_numberOfHotelsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_numberOfHotelsBox")));
				logger.info("Hotel Results page checkbox number of hotels available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page checkbox number of hotels not available ",e);
			}
			return element;
		}
		
		public static WebElement label_itemsPerPage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_itemsPerPage")));
				logger.info("Hotel Results page label items per page available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label items per page not available ",e);
			}
			return element;
		}
		
		public static WebElement label_showingHotelCount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_showingHotelCount")));
				logger.info("Hotel Results page label showing hotel count available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label showing hotel count not available ",e);
			}
			return element;
		}
		
		public static WebElement link_paginationNumsButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_paginationNumsButton")));
				logger.info("Hotel Results page pagination numbers link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page pagination numbers link button not available ",e);
			}
			return element;
		}
		
		public static WebElement link_paginationNumsLastButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_paginationNumsLastButton")));
				logger.info("Hotel Results page pagination numbers last link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page pagination numbers last link button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_hotelImageButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_hotelImageButton")));
				logger.info("Hotel Results page hotel image button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page hotel image button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_hotelpromotion(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelpromotion")));
				logger.info("Hotel Results page label hotel promotion available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label hotel promotion not available ",e);
			}
			return element;
		}
		
		public static WebElement label_hotelstarRating(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelstarRating")));
				logger.info("Hotel Results page label hotel star rating available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label hotel star rating not available ",e);
			}
			return element;
		}
		
		public static WebElement link_hotelresultHotelNameButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_hotelresultHotelNameButton")));
				logger.info("Hotel Results page hotel result hotel name link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page hotel result hotel name link button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_hotelAddress(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelAddress")));
				logger.info("Hotel Results page label hotel address available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label hotel address not available ",e);
			}
			return element;
		}
		
		public static WebElement label_hotelPrice(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelPrice")));
				logger.info("Hotel Results page label hotel price available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label hotel price not available ",e);
			}
			return element;
		}
		
		public static WebElement link_moreButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_moreButton")));
				logger.info("Hotel Results page More link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page More link button not available ",e);
			}
			return element;
		}
		
		public static WebElement link_moreInfoButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_moreInfoButton")));
				logger.info("Hotel Results page More Info link button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page More Info link button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_viewOtherRoomsButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_viewOtherRoomsButton")));
				logger.info("Hotel Results page view other rooms button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page view other rooms button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_moreInfoButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_moreInfoButton")));
				logger.info("Hotel Results page More Info button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page More Info button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_bookNowButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_bookNowButton")));
				logger.info("Hotel Results page book now button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page book now button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_addToCartAndContinueButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_addToCartAndContinueButton")));
				logger.info("Hotel Results page add to cart and continue button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page add to cart and continue button not available ",e);
			}
			return element;
		}
		
		public static WebElement label_room(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_room")));
				logger.info("Hotel Results page label room available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label room not available ",e);
			}
			return element;
		}
		
		public static WebElement label_roomTypeBedType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomTypeBedType")));
				logger.info("Hotel Results page label room type/ bed type available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label room type/ bed type not available ",e);
			}
			return element;
		}
		
		public static WebElement label_mealPlan(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_mealPlan")));
				logger.info("Hotel Results page label meal plan available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label meal plan not available ",e);
			}
			return element;
		}
		
		public static WebElement label_moreInfoHotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_moreInfoHotelName")));
				logger.info("Hotel Results page label more Info Hotel Name available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page label more Info Hotel Name not available ",e);
			}
			return element;
		}
		
		public static WebElement button_moreInfoHotelDescriptionButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_moreInfoHotelDescriptionButton")));
				logger.info("Hotel Results page more Info hotel description button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page more Info hotel description button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_moreInfoHotelImagesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_moreInfoHotelImagesButton")));
				logger.info("Hotel Results page more Info hotel images button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page more Info hotel images button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_moreInfoHotelMapButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_moreInfoHotelMapButton")));
				logger.info("Hotel Results page more Info hotel map button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page more Info hotel map button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_moreInfoHotelAmenitiesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_moreInfoHotelAmenitiesButton")));
				logger.info("Hotel Results page more Info hotel amenities button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page more Info hotel amenities button not available ",e);
			}
			return element;
		}
		
		public static WebElement button_moreInfoCloseButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("HotelResultsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_moreInfoCloseButton")));
				logger.info("Hotel Results page more Info hotel Close button available");
			} catch (Exception e) {
				logger.fatal("Hotel Results page more Info hotel Close button not available ",e);
			}
			return element;
		}
	}
	
	
// Review Page ################################################################
	public static class PackageReviewPage{
		public static WebElement img_aviancaLogo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_aviancaLogo")));
				logger.info("Package Review page image avianca logo available");
			} catch (Exception e) {
				logger.fatal("Package Review page image avianca logo not available",e);
			}
			return element;
		}
		
		public static WebElement link_homeButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_homeButton")));
				logger.info("Package Review page home link button available");
			} catch (Exception e) {
				logger.fatal("Package Review page home link button not available",e);
			}
			return element;
		}
		
		public static WebElement img_searchPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_searchPanel")));
				logger.info("Package Review page search panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page search panel image not available",e);
			}
			return element;
		}
		
		public static WebElement img_selectionPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectionPanel")));
				logger.info("Package Review page selection panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page selection panel image not available",e);
			}
			return element;
		}
		
		public static WebElement img_reviewPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_reviewPanel")));
				logger.info("Package Review page review panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page review panel image not available",e);
			}
			return element;
		}
		
		public static WebElement img_confirmPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_confirmPanel")));
				logger.info("Package Review page confirm panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page confirm panel image not available",e);
			}
			return element;
		}
		
		public static WebElement button_changeFlightButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_changeFlightButton")));
				logger.info("Package Review page change flight button available");
			} catch (Exception e) {
				logger.fatal("Package Review page change flight button not available",e);
			}
			return element;
		}
		
		public static WebElement button_addHotelButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_addHotelButton")));
				logger.info("Package Review page add hotel button available");
			} catch (Exception e) {
				logger.fatal("Package Review page add hotel button not available",e);
			}
			return element;
		}
		
		public static WebElement button_removeHotelButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_removeHotelButton")));
				logger.info("Package Review page remove hotel button available");
			} catch (Exception e) {
				logger.fatal("Package Review page remove hotel button not available",e);
			}
			return element;
		}
		
		public static WebElement button_selectCarButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_selectCarButton")));
				logger.info("Package Review page select car button available");
			} catch (Exception e) {
				logger.fatal("Package Review page select car button not available",e);
			}
			return element;
		}
		
		public static WebElement button_selectActivityButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_selectActivityButton")));
				logger.info("Package Review page select activity button available");
			} catch (Exception e) {
				logger.fatal("Package Review page select activity button not available",e);
			}
			return element;
		}
		
		public static WebElement button_bookNowButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_bookNowButton")));
				logger.info("Package Review page book now button available");
			} catch (Exception e) {
				logger.fatal("Package Review page book now button not available",e);
			}
			return element;
		}
		
		public static WebElement label_totalAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_totalAmount")));
				logger.info("Package Review page label total amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label total amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightsection(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightsection")));
				logger.info("Package Review page label flight section available");
			} catch (Exception e) {
				logger.fatal("Package Review page label flight section not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDescription")));
				logger.info("Package Review page label depart description available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart description not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFlightNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFlightNumber")));
				logger.info("Package Review page label depart flight number available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart flight number not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDate")));
				logger.info("Package Review page label depart date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart date not available",e);
			}
			return element;
		}
		
		public static WebElement label_departTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departTime")));
				logger.info("Package Review page label depart time available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart time not available",e);
			}
			return element;
		}
		
		public static WebElement label_departAirport(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departAirport")));
				logger.info("Package Review page label depart airport available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart airport not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDuration(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDuration")));
				logger.info("Package Review page label depart duration available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart duration not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFlightClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFlightClass")));
				logger.info("Package Review page label depart flight class available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart flight class not available",e);
			}
			return element;
		}
		
		public static WebElement label_departInfoPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departInfoPanel")));
				logger.info("Package Review page label depart info panel available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart info panel not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDescription")));
				logger.info("Package Review page label return description available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return description not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFlightNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFlightNumber")));
				logger.info("Package Review page label return flight number available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return flight number not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDate")));
				logger.info("Package Review page label return date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return date not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnTime")));
				logger.info("Package Review page label return time available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return time not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnAirport(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnAirport")));
				logger.info("Package Review page label return airport available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return airport not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDuration(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDuration")));
				logger.info("Package Review page label return duration available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return duration not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFlightClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFlightClass")));
				logger.info("Package Review page label return flight class available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return flight class not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnInfoPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnInfoPanel")));
				logger.info("Package Review page label return info panel available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return info not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerType")));
				logger.info("Package Review page label passenger type available");
			} catch (Exception e) {
				logger.fatal("Package Review page label passenger type not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfPax(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfPax")));
				logger.info("Package Review page label number of pax available");
			} catch (Exception e) {
				logger.fatal("Package Review page label number of pax not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFareRules(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFareRules")));
				logger.info("Package Review page label depart fare rules available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart fare rules not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFareRules(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFareRules")));
				logger.info("Package Review page label return fare rules available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return fare rules not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightPassengerInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightPassengerInfo")));
				logger.info("Package Review page label flight passenger info available");
			} catch (Exception e) {
				logger.fatal("Package Review page label flight passenger info not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSection(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSection")));
				logger.info("Package Review page label hotel section available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel section not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelImage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelImage")));
				logger.info("Package Review page label hotel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel image not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelName")));
				logger.info("Package Review page label hotel name available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel name not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelAddress(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelAddress")));
				logger.info("Package Review page label hotel address available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel address not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckInDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckInDate")));
				logger.info("Package Review page label hotel check in date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel check in date not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckOutDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckOutDate")));
				logger.info("Package Review page label hotel check out date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel check out date not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelNightCount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelNightCount")));
				logger.info("Package Review page label hotel night count available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel night count not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomCount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomCount")));
				logger.info("Package Review page label hotel room count available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room count not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRemoveButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRemoveButton")));
				logger.info("Package Review page label hotel remove button available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel remove button not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoom(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoom")));
				logger.info("Package Review page label hotel room available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomType")));
				logger.info("Package Review page label hotel room type available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelBedType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelBedType")));
				logger.info("Package Review page label hotel bed type available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel bed type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelMealPlan(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelMealPlan")));
				logger.info("Package Review page label hotel meal plan available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel meal plan not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomInfo")));
				logger.info("Package Review page label hotel room info available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room info not available",e);
			}
			return element;
		}
		
		public static WebElement label_buttonContinueButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_buttonContinueButton")));
				logger.info("Package Review page button continue available");
			} catch (Exception e) {
				logger.fatal("Package Review page button continue not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentDetails")));
				logger.info("Package Review page label payment details available");
			} catch (Exception e) {
				logger.fatal("Package Review page label payment details not available",e);
			}
			return element;
		}
		
		public static WebElement label_priceUnit(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceUnit")));
				logger.info("Package Review page label price unit available");
			} catch (Exception e) {
				logger.fatal("Package Review page label price unit not available",e);
			}
			return element;
		}
		
		public static WebElement label_serviceValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_serviceValue")));
				logger.info("Package Review page label service value available");
			} catch (Exception e) {
				logger.fatal("Package Review page label service value not available",e);
			}
			return element;
		}
		
		public static WebElement label_serviceAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_serviceAmount")));
				logger.info("Package Review page label service amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label service amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_adminFee(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adminFee")));
				logger.info("Package Review page label admin fee available");
			} catch (Exception e) {
				logger.fatal("Package Review page label admin fee not available",e);
			}
			return element;
		}
		
		public static WebElement label_adminFeeAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adminFeeAmount")));
				logger.info("Package Review page label admin fee amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label admin fee amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_taxes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_taxes")));
				logger.info("Package Review page label taxes available");
			} catch (Exception e) {
				logger.fatal("Package Review page label taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_taxesAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_taxesAmount")));
				logger.info("Package Review page label taxes amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label taxes amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_foodAndBeverages(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_foodAndBeverages")));
				logger.info("Package Review page label food and beverages available");
			} catch (Exception e) {
				logger.fatal("Package Review page label food and beverages not available",e);
			}
			return element;
		}
		
		public static WebElement label_foodAndBeveragesAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_foodAndBeveragesAmount")));
				logger.info("Package Review page label food and beverages amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label food and beverages amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_otherTaxes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_otherTaxes")));
				logger.info("Package Review page label other taxes available");
			} catch (Exception e) {
				logger.fatal("Package Review page label other taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_otherTaxesAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_otherTaxesAmount")));
				logger.info("Package Review page label other taxes amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label other taxes amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_overallValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_overallValue")));
				logger.info("Package Review page label overall value available");
			} catch (Exception e) {
				logger.fatal("Package Review page label overall value not available",e);
			}
			return element;
		}
		
		public static WebElement label_overallValueAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_overallValueAmount")));
				logger.info("Package Review page label overall value amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label overall value amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_priceByPerson(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceByPerson")));
				logger.info("Package Review page label price by person available");
			} catch (Exception e) {
				logger.fatal("Package Review page label price by person not available",e);
			}
			return element;
		}
		
		public static WebElement label_priceByPersonAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceByPersonAmount")));
				logger.info("Package Review page label price by person amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label price by person amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_spanishText(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_spanishText")));
				logger.info("Package Review page label spanish text available");
			} catch (Exception e) {
				logger.fatal("Package Review page label spanish text not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryHeader(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryHeader")));
				logger.info("Package Review page label supplementary header available");
			} catch (Exception e) {
				logger.fatal("Package Review page label spanish text not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryName")));
				logger.info("Package Review page label supplementary name available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary name not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryRate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryRate")));
				logger.info("Package Review page label supplementary rate available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary rate not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementarySelection(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementarySelection")));
				logger.info("Package Review page label supplementary selection available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary selection not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryInfo")));
				logger.info("Package Review page label supplementary info available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary info not available",e);
			}
			return element;
		}
		
		public static WebElement label_cancellationPolicy(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_cancellationPolicy")));
				logger.info("Package Review page label cancellation policy available");
			} catch (Exception e) {
				logger.fatal("Package Review page label cancellation policy not available",e);
			}
			return element;
		}
		
		public static WebElement check_termsAndConditionsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_termsAndConditionsBox")));
				logger.info("Package Review page terms and conditions check box available");
			} catch (Exception e) {
				logger.fatal("Package Review page terms and conditions check box not available",e);
			}
			return element;
		}
		
		public static WebElement label_termsAndCondition(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_termsAndCondition")));
				logger.info("Package Review page label terms and conditions available");
			} catch (Exception e) {
				logger.fatal("Package Review page label terms and conditions not available",e);
			}
			return element;
		}
	}
	
	
	
// ##################Package Occupancy Page #############################################################
	
	public static class PackageOccupancyPage{
		
		public static WebElement img_searchPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_searchPanel")));
				logger.info("Package Occupancy page image search panel available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page image search panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_selectionPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectionPanel")));
				logger.info("Package Occupancy Page image selection panel available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page image selection panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_reviewPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_reviewPanel")));
				logger.info("Package Occupancy Page image review panel available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page image review panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_confirmPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_confirmPanel")));
				logger.info("Package Occupancy Page image confirm panel available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page image confirm panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_flightPassenger(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_flightPassenger")));
				logger.info("Package Occupancy Page image flight passenger available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page image flight passenger not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightPassengerInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightPassengerInfo")));
				logger.info("Package Occupancy Page label flight passenger info available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger info not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightPassengerFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightPassengerFName")));
				logger.info("Package Occupancy Page label flight passenger first name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger first name not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightPassengerLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightPassengerLName")));
				logger.info("Package Occupancy Page label flight passenger last name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger last name not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightDoB(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightDoB")));
				logger.info("Package Occupancy Page label flight passenger date of birth available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger date of birth not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightContactInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightContactInfo")));
				logger.info("Package Occupancy Page label flight passenger contact info available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger contact info not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightEmail(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightEmail")));
				logger.info("Package Occupancy Page label flight passenger email available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger email not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightLifeMilesNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightLifeMilesNumber")));
				logger.info("Package Occupancy Page label flight passenger life miles number available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger life miles number not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightAPISDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightAPISDetails")));
				logger.info("Package Occupancy Page label flight passenger APIS details available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label flight passenger APIS details not available",e);
			}
			return element;
		}
		
		public static WebElement text_flightPassengerFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_flightPassengerFName")));
				logger.info("Package Occupancy Page textbox flight passenger first name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox flight passenger first name not available",e);
			}
			return element;
		}
		
		public static WebElement text_flightPassengerLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_flightPassengerLName")));
				logger.info("Package Occupancy Page textbox flight passenger last name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox flight passenger last name not available",e);
			}
			return element;
		}
		
		public static WebElement drop_birthDateBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_birthDateBox")));
				logger.info("Package Occupancy Page dropdown birth date box available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown birth date box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_birthMonthBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_birthMonthBox")));
				logger.info("Package Occupancy Page dropdown birth month box available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown birth month box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_birthYearBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_birthYearBox")));
				logger.info("Package Occupancy Page dropdown birth year box available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown birth year box not available",e);
			}
			return element;
		}
		
		public static WebElement text_contactInfoBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_contactInfoBox")));
				logger.info("Package Occupancy Page textbox contact info available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox contact info not available",e);
			}
			return element;
		}
		
		public static WebElement text_emailBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_emailBox")));
				logger.info("Package Occupancy Page textbox email available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox email not available",e);
			}
			return element;
		}
		
		public static WebElement text_lifeMilesNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_lifeMilesNumber")));
				logger.info("Package Occupancy Page textbox lifemiles number available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox lifemiles number not available",e);
			}
			return element;
		}
		
		public static WebElement label_passportNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passportNumber")));
				logger.info("Package Occupancy Page label passport number available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label passport number not available",e);
			}
			return element;
		}
		
		public static WebElement label_passportExpireDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passportExpireDate")));
				logger.info("Package Occupancy Page label passport expire date available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label passport expire date not available",e);
			}
			return element;
		}
		
		public static WebElement label_passportIssuedCountry(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passportIssuedCountry")));
				logger.info("Package Occupancy Page label passport issued country available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label passport issued country not available",e);
			}
			return element;
		}
		
		public static WebElement label_gender(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_gender")));
				logger.info("Package Occupancy Page label gender available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label gender not available",e);
			}
			return element;
		}
		
		public static WebElement label_nationality(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_nationality")));
				logger.info("Package Occupancy Page label nationality available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label nationality not available",e);
			}
			return element;
		}
		
		public static WebElement label_redressNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_redressNumber")));
				logger.info("Package Occupancy Page label redress available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label redress not available",e);
			}
			return element;
		}
		
		public static WebElement text_passportNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_passportNumber")));
				logger.info("Package Occupancy Page textbox passport number available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox passport number not available",e);
			}
			return element;
		}
		
		public static WebElement drop_expireDateBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_expireDateBox")));
				logger.info("Package Occupancy Page dropdown expire date available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown expire date not available",e);
			}
			return element;
		}
		
		public static WebElement drop_expireMonthBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_expireMonthBox")));
				logger.info("Package Occupancy Page dropdown expire month available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown expire month not available",e);
			}
			return element;
		}
		
		public static WebElement drop_expireYearBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_expireYearBox")));
				logger.info("Package Occupancy Page dropdown expire year available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown expire year not available",e);
			}
			return element;
		}
		
		public static WebElement drop_passportIssuedCountryBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_passportIssuedCountryBox")));
				logger.info("Package Occupancy Page dropdown passport issued country available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown passport issued country not available",e);
			}
			return element;
		}
		
		public static WebElement drop_genderBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_genderBox")));
				logger.info("Package Occupancy Page dropdown gender available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown gender not available",e);
			}
			return element;
		}
		
		public static WebElement drop_nationalityBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_nationalityBox")));
				logger.info("Package Occupancy Page dropdown nationality available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page dropdown nationality not available",e);
			}
			return element;
		}
		
		public static WebElement text_redressnumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_redressnumber")));
				logger.info("Package Occupancy Page textbox redress number available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox redress number not available",e);
			}
			return element;
		}
		
		public static WebElement link_learnMoreButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_learnMoreButton")));
				logger.info("Package Occupancy Page link learn more available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page link learn more not available",e);
			}
			return element;
		}
		
		public static WebElement label_tsaInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_tsaInfo")));
				logger.info("Package Occupancy Page label TSA info available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label TSA info not available",e);
			}
			return element;
		}
		
		public static WebElement button_flightAPISDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_flightAPISDetails")));
				logger.info("Package Occupancy Page button APIS details available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page button APIS details not available",e);
			}
			return element;
		}
		
		public static WebElement img_hotelPassenger(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_hotelPassenger")));
				logger.info("Package Occupancy Page image hotel passenger available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page image hotel passenger not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelName")));
				logger.info("Package Occupancy Page label hotel name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label hotel name not available",e);
			}
			return element;
		}
		
		public static WebElement label_room(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_room")));
				logger.info("Package Occupancy Page label room available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label room not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomNumber")));
				logger.info("Package Occupancy Page label room number available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label room number not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelAdult1(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelAdult1")));
				logger.info("Package Occupancy Page label hotel adult 1 available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label hotel adult 1 not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelPassengerFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelPassengerFName")));
				logger.info("Package Occupancy Page label hotel passenger first name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label hotel passenger first name not available",e);
			}
			return element;
		}
		
		public static WebElement text_hotelPassengerFNameBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_hotelPassengerFNameBox")));
				logger.info("Package Occupancy Page textbox hotel passenger first name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox hotel passenger first name not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelPassengerLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelPassengerLName")));
				logger.info("Package Occupancy Page label hotel passenger last name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label hotel passenger last name not available",e);
			}
			return element;
		}
		
		public static WebElement text_hotelPassengerLNameBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_hotelPassengerLNameBox")));
				logger.info("Package Occupancy Page textbox hotel passenger last name available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page textbox hotel passenger last name not available",e);
			}
			return element;
		}
		
		public static WebElement check_privacyPolicyBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_privacyPolicyBox")));
				logger.info("Package Occupancy Page checkbox privacy policy available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page checkbox privacy policy not available",e);
			}
			return element;
		}
		
		public static WebElement link_generalPrivacyPolicyButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_generalPrivacyPolicyButton")));
				logger.info("Package Occupancy Page link button general privacy policy available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page link button general privacy policy not available",e);
			}
			return element;
		}
		
		public static WebElement link_conditionsOfAccess(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_conditionsOfAccess")));
				logger.info("Package Occupancy Page link button condition of access available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page link button condition of access not available",e);
			}
			return element;
		}
		
		public static WebElement button_continueToPayButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_continueToPayButton")));
				logger.info("Package Occupancy Page button continue to pay available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page button continue to pay not available",e);
			}
			return element;
		}
		
		public static WebElement img_alertMessageImage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_alertMessageImage")));
				logger.info("Package Occupancy Page alert message image available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page alert message image not available",e);
			}
			return element;
		}
		
		public static WebElement label_alertMessage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_alertMessage")));
				logger.info("Package Occupancy Page label alert message available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page label alert message not available",e);
			}
			return element;
		}
		
		public static WebElement button_alertMessageYesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_alertMessageYesButton")));
				logger.info("Package Occupancy Page button alert message yes available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page button alert message yes not available",e);
			}
			return element;
		}
		
		public static WebElement button_alertMessageNoButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageOccupancyPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_alertMessageNoButton")));
				logger.info("Package Occupancy Page button alert message no available");
			} catch (Exception e) {
				logger.fatal("Package Occupancy Page button alert message no not available",e);
			}
			return element;
		}
	}
	
	public static class PaymentOptionsPage{
		
		public static WebElement label_totalAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_totalAmount")));
				logger.info("Payment Options Page label total amount available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label total amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_totalAmountValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_totalAmountValue")));
				logger.info("Payment Options Page label total amount value available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label total amount value not available",e);
			}
			return element;
		}
		
		public static WebElement radio_debitCardButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_debitCardButton")));
				logger.info("Payment Options Page radio button debit card available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio button debit card not available",e);
			}
			return element;
		}
		
		public static WebElement label_debit(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debit")));
				logger.info("Payment Options Page label debit available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit not available",e);
			}
			return element;
		}
		
		public static WebElement radio_creditCardButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_creditCardButton")));
				logger.info("Payment Options Page radio button credit available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio button credit not available",e);
			}
			return element;
		}
		
		public static WebElement label_credit(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_credit")));
				logger.info("Payment Options Page label credit available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit not available",e);
			}
			return element;
		}
		
		public static WebElement img_alertMessageAcceptImage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_alertMessageAcceptImage")));
				logger.info("Payment Options Page image alert message accept available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page image alert message accept not available",e);
			}
			return element;
		}
		
		public static WebElement label_alertMessageAcceptDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_alertMessageAcceptDescription")));
				logger.info("Payment Options Page label alert message accept description available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label alert message accept description not available",e);
			}
			return element;
		}
		
		public static WebElement button_alertMessageAcceptButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_alertMessageAcceptButton")));
				logger.info("Payment Options Page button alert message accept available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page button alert message accept not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitAccountInformation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitAccountInformation")));
				logger.info("Payment Options Page label debit account information available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit account information not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitFName")));
				logger.info("Payment Options Page label debit first name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit first name not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitLName")));
				logger.info("Payment Options Page label debit last name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit last name not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitPhone(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitPhone")));
				logger.info("Payment Options Page label debit phone number available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit phone number not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitEmail(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitEmail")));
				logger.info("Payment Options Page label debit Email available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit Email not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitIDType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitIDType")));
				logger.info("Payment Options Page label debit ID type available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit ID type not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitIDNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitIDNumber")));
				logger.info("Payment Options Page label debit ID number available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit ID number not available",e);
			}
			return element;
		}
		
		public static WebElement text_debitFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_debitFName")));
				logger.info("Payment Options Page text debit first name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit first name not available",e);
			}
			return element;
		}
		
		public static WebElement text_debitLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_debitLName")));
				logger.info("Payment Options Page text debit last name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit last name not available",e);
			}
			return element;
		}
		
		public static WebElement text_debitPhone(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_debitPhone")));
				logger.info("Payment Options Page text debit phone number available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit phone number not available",e);
			}
			return element;
		}
		
		public static WebElement text_debitEmail(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_debitEmail")));
				logger.info("Payment Options Page text debit email available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit email not available",e);
			}
			return element;
		}
		
		public static WebElement drop_debitIDTypeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_debitIDTypeBox")));
				logger.info("Payment Options Page text debit ID type available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit ID type not available",e);
			}
			return element;
		}
		
		public static WebElement text_debitIDNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_debitIDNumber")));
				logger.info("Payment Options Page text debit ID number available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit ID number not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitImportantNotice(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitImportantNotice")));
				logger.info("Payment Options Page label debit Importance notice available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit Importance notice not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitBankName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitBankName")));
				logger.info("Payment Options Page label debit bank name notice available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit bank name not available",e);
			}
			return element;
		}
		
		public static WebElement text_debitBankName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_debitBankName")));
				logger.info("Payment Options Page text debit bank name notice available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text debit bank name not available",e);
			}
			return element;
		}
		
		public static WebElement radio_debitIndividualButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_debitIndividualButton")));
				logger.info("Payment Options Page radio button debit individual notice available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio button debit individual not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitIndividual(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitIndividual")));
				logger.info("Payment Options Page label debit individual available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit individual not available",e);
			}
			return element;
		}
		
		public static WebElement radio_debitBusiness(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_debitBusiness")));
				logger.info("Payment Options Page radio button debit business available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio button debit business not available",e);
			}
			return element;
		}
		
		public static WebElement label_debitBusiness(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_debitBusiness")));
				logger.info("Payment Options Page label debit business available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label debit business not available",e);
			}
			return element;
		}
		
		public static WebElement check_debitagreeToTermsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_debitagreeToTermsBox")));
				logger.info("Payment Options Page check box debit agree to terms available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page check box debit agree to terms not available",e);
			}
			return element;
		}
		
		public static WebElement link_debitagreeToTermsButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_debitagreeToTermsButton")));
				logger.info("Payment Options Page link debit agree to terms button available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page link debit agree to terms button not available",e);
			}
			return element;
		}
		
		public static WebElement button_debitPayNowButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_debitPayNowButton")));
				logger.info("Payment Options Page button debit pay now available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page button debit pay now not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditCardHolderInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditCardHolderInfo")));
				logger.info("Payment Options Page label credit card holder info available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit card holder info not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditFName")));
				logger.info("Payment Options Page label credit first name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit first name not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditLName")));
				logger.info("Payment Options Page label credit last name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit last name not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditEmail(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditEmail")));
				logger.info("Payment Options Page label credit Email available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit Email not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditVerifyEmail(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditVerifyEmail")));
				logger.info("Payment Options Page label credit verify Email available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit verify Email not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditIDType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditIDType")));
				logger.info("Payment Options Page label credit ID type available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit ID type not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditIDNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditIDNumber")));
				logger.info("Payment Options Page label credit ID number available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit ID number not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditFNameBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditFNameBox")));
				logger.info("Payment Options Page text credit first name box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit first name box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditLNameBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditLNameBox")));
				logger.info("Payment Options Page text credit last name box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit last name box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditEmailBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditEmailBox")));
				logger.info("Payment Options Page text credit Email box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit Email box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditVerifyEmailBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditVerifyEmailBox")));
				logger.info("Payment Options Page text credit verify Email box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit verify Email box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_creditIDTypeBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_creditIDTypeBox")));
				logger.info("Payment Options Page drop credit ID type box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page drop credit ID type box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditIDNumberBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditIDNumberBox")));
				logger.info("Payment Options Page text credit ID number box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit ID number box not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditAddressInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditAddressInfo")));
				logger.info("Payment Options Page label credit address info available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit address info not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditAddress(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditAddress")));
				logger.info("Payment Options Page label credit address available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit address not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditCity(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditCity")));
				logger.info("Payment Options Page label credit city available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit city not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditState(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditState")));
				logger.info("Payment Options Page label credit state available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit state not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditAddressBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditAddressBox")));
				logger.info("Payment Options Page text credit address box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit address box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditCityBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditCityBox")));
				logger.info("Payment Options Page text credit city box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit city box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditStateBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditStateBox")));
				logger.info("Payment Options Page text credit state box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit state box not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditZipCode(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditZipCode")));
				logger.info("Payment Options Page label credit ZIP code available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit ZIP code not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditCountry(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditCountry")));
				logger.info("Payment Options Page label credit country available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit country not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditPhone(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditPhone")));
				logger.info("Payment Options Page label credit phone available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit phone not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditZipCode(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditZipCode")));
				logger.info("Payment Options Page text credit ZIP code box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit ZIP code box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_creditCountryBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_creditCountryBox")));
				logger.info("Payment Options Page drop credit country box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page drop credit country box not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditPhoneBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditPhoneBox")));
				logger.info("Payment Options Page text credit phone box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit phone box not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditCardInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditCardInfo")));
				logger.info("Payment Options Page label credit card info available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit card info not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditPaymentMethod(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditPaymentMethod")));
				logger.info("Payment Options Page label credit payment method available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit payment method not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditPaymentMethodDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditPaymentMethodDescription")));
				logger.info("Payment Options Page label credit payment method description available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit payment method description not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditSelectPaymentType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditSelectPaymentType")));
				logger.info("Payment Options Page label credit select payment type available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit select payment type not available",e);
			}
			return element;
		}
		
		public static WebElement radio_creditCardTypeDinersButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_creditCardTypeDinersButton")));
				logger.info("Payment Options Page radio credit card type Diners button available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio credit card type Diners button not available",e);
			}
			return element;
		}
		
		public static WebElement img_creditDiners(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_creditDiners")));
				logger.info("Payment Options Page image credit card type Diners available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page image credit card type Diners not available",e);
			}
			return element;
		}
		
		public static WebElement radio_creditCardTypeMasterButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_creditCardTypeMasterButton")));
				logger.info("Payment Options Page radio credit card type Master button available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio credit card type Master button not available",e);
			}
			return element;
		}
		
		public static WebElement img_creditMaster(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_creditMaster")));
				logger.info("Payment Options Page image credit card type Master available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page image credit card type Master not available",e);
			}
			return element;
		}
		
		public static WebElement radio_creditCardTypeVisa(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_creditCardTypeVisa")));
				logger.info("Payment Options Page radio credit card type Visa available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio credit card type Visa not available",e);
			}
			return element;
		}
		
		public static WebElement img_creditVisa(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_creditVisa")));
				logger.info("Payment Options Page image credit card type Visa available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page image credit card type Visa not available",e);
			}
			return element;
		}
		
		public static WebElement radio_creditCardAmex(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("radio_creditCardAmex")));
				logger.info("Payment Options Page radio credit card type AMEX available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page radio credit card type AMEX not available",e);
			}
			return element;
		}
		
		public static WebElement img_creditAmex(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_creditAmex")));
				logger.info("Payment Options Page image credit card type AMEX available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page image credit card type AMEX not available",e);
			}
			return element;
		}
		
		public static WebElement label_creditCardNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_creditCardNumber")));
				logger.info("Payment Options Page label credit card number available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit card number not available",e);
			}
			return element;
		}
		
		public static WebElement label_expireDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_expireDate")));
				logger.info("Payment Options Page label credit card number expire date available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit card number expire date not available",e);
			}
			return element;
		}
		
		public static WebElement label_quotes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_quotes")));
				logger.info("Payment Options Page label credit quotes available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit quotes not available",e);
			}
			return element;
		}
		
		public static WebElement label_installments(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_installments")));
				logger.info("Payment Options Page label credit installments available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit installments not available",e);
			}
			return element;
		}
		
		public static WebElement text_creditCardNumberBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_creditCardNumberBox")));
				logger.info("Payment Options Page label credit installments available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit installments not available",e);
			}
			return element;
		}
		
		public static WebElement drop_expireMonthBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_expireMonthBox")));
				logger.info("Payment Options Page drop credit expire month box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page drop credit expire month box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_expireYearBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_expireYearBox")));
				logger.info("Payment Options Page drop credit expire year box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page drop credit expire year box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_QuoteBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_QuoteBox")));
				logger.info("Payment Options Page drop credit quote box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page drop credit quote box not available",e);
			}
			return element;
		}
		
		public static WebElement drop_installmentsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("drop_installmentsBox")));
				logger.info("Payment Options Page drop credit installments box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page drop credit installments box not available",e);
			}
			return element;
		}
		
		public static WebElement label_cardHolderName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_cardHolderName")));
				logger.info("Payment Options Page label credit card holder name available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit card holder name not available",e);
			}
			return element;
		}
		
		public static WebElement label_cvv(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_cvv")));
				logger.info("Payment Options Page label credit cvv available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label credit cvv not available",e);
			}
			return element;
		}
		
		public static WebElement text_cardHolderNameBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("text_cardHolderNameBox")));
				logger.info("Payment Options Page text credit card holder name box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit card holder name box not available",e);
			}
			return element;
		}
		
		public static WebElement textCvvBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("textCvvBox")));
				logger.info("Payment Options Page text credit cvv box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page text credit cvv box not available",e);
			}
			return element;
		}
		
		public static WebElement link_whatIsThisButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_whatIsThisButton")));
				logger.info("Payment Options Page link what is this button available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page link what is this button not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentSecured(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentSecured")));
				logger.info("Payment Options Page label payment secured available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page label paymentsecured not available",e);
			}
			return element;
		}
		
		public static WebElement check_agreeToTermsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_agreeToTermsBox")));
				logger.info("Payment Options Page check agree to terms box available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page check agree to terms box not available",e);
			}
			return element;
		}
		
		public static WebElement link_termsAndConditions(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_termsAndConditions")));
				logger.info("Payment Options Page link terms and conditions button available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page link terms and conditions button not available",e);
			}
			return element;
		}
		
		public static WebElement button_creditPayNow(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PaymentOptionsPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_creditPayNow")));
				logger.info("Payment Options Page button pay now available");
			} catch (Exception e) {
				logger.fatal("Payment Options Page button pay now not available",e);
			}
			return element;
		}
	}
	
	
//Confirmation Page
	public static class ConfirmationPage{
		
		public static WebElement img_searchPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_searchPanel")));
				logger.info("Confirmation Page image search panel available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page image search panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_selectionPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectionPanel")));
				logger.info("Confirmation Page image selection panel available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page image selection panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_reviewPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_reviewPanel")));
				logger.info("Confirmation Page image review panel available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page image review panel not available",e);
			}
			return element;
		}
		
		public static WebElement img_confirmPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_confirmPanel")));
				logger.info("Confirmation Page image confirm panel available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page image confirm panel not available",e);
			}
			return element;
		}
		
		public static WebElement label_bookingConfirmation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_bookingConfirmation")));
				logger.info("Confirmation Page label Booking confirmation available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label Booking confirmation not available",e);
			}
			return element;
		}
		
		public static WebElement label_bookingConfirmationNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_bookingConfirmationNumber")));
				logger.info("Confirmation Page label Booking confirmation number available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label Booking confirmation number not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentStatus(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentStatus")));
				logger.info("Confirmation Page label payment status available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment status not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentStatusType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentStatusType")));
				logger.info("Confirmation Page label payment status type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment status type not available",e);
			}
			return element;
		}
		
		public static WebElement label_tickectsAndVouchers(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_tickectsAndVouchers")));
				logger.info("Confirmation Page label ticket and vouchers available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label ticket and vouchers not available",e);
			}
			return element;
		}
		
		public static WebElement label_ticketsAndVoucherStatus(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_ticketsAndVoucherStatus")));
				logger.info("Confirmation Page label ticket and vouchers status available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label ticket and vouchers status not available",e);
			}
			return element;
		}
		
		public static WebElement label_bookingDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_bookingDescription")));
				logger.info("Confirmation Page label booking type description available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label booking type description not available",e);
			}
			return element;
		}
		
		public static WebElement label_tripItinararyDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_tripItinararyDetails")));
				logger.info("Confirmation Page label trip itinarary details available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label trip itinarary details not available",e);
			}
			return element;
		}
		
		public static WebElement label_pnrNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_pnrNumber")));
				logger.info("Confirmation Page label pnr number available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label pnr number not available",e);
			}
			return element;
		}
		
		public static WebElement label_pnrNumberValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_pnrNumberValue")));
				logger.info("Confirmation Page label pnr number value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label pnr number value not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDate")));
				logger.info("Confirmation Page label departure date available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure date not available",e);
			}
			return element;
		}
		
		public static WebElement label_departLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departLocation")));
				logger.info("Confirmation Page label departure location available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure location not available",e);
			}
			return element;
		}
		
		public static WebElement label_departCountry(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departCountry")));
				logger.info("Confirmation Page label departure country available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure country not available",e);
			}
			return element;
		}
		
		public static WebElement label_departArrivalLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departArrivalLocation")));
				logger.info("Confirmation Page label departure arrival location available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure arrival location not available",e);
			}
			return element;
		}
		
		public static WebElement label_departArrivalCountry(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departArrivalCountry")));
				logger.info("Confirmation Page label departure arrival country available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure arrival country not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFlight(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFlight")));
				logger.info("Confirmation Page label departure flight available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure flight not available",e);
			}
			return element;
		}
		
		public static WebElement label_departAndArrival(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departAndArrival")));
				logger.info("Confirmation Page label departure and arrival available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure and arrival not available",e);
			}
			return element;
		}
		
		public static WebElement label_departTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departTime")));
				logger.info("Confirmation Page label departure time available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure time not available",e);
			}
			return element;
		}
		
		public static WebElement label_departAirport(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departAirport")));
				logger.info("Confirmation Page label departure airport available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure airport not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDuration(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDuration")));
				logger.info("Confirmation Page label departure duration available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure duration not available",e);
			}
			return element;
		}
		
		public static WebElement label_departClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departClass")));
				logger.info("Confirmation Page label departure class available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure class not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFlightNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFlightNumber")));
				logger.info("Confirmation Page label departure flight number available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure flight number not available",e);
			}
			return element;
		}
		
		public static WebElement label_departAndArrivalValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departAndArrivalValue")));
				logger.info("Confirmation Page label departure and arrival value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure and arrival value not available",e);
			}
			return element;
		}
		
		public static WebElement label_departTimeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departTimeValue")));
				logger.info("Confirmation Page label departure time value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure time value not available",e);
			}
			return element;
		}
		
		public static WebElement label_departAirportValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departAirportValue")));
				logger.info("Confirmation Page label departure airport value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure airport value not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDurationValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDurationValue")));
				logger.info("Confirmation Page label departure value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure value not available",e);
			}
			return element;
		}
		
		public static WebElement label_departClassValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departClassValue")));
				logger.info("Confirmation Page label departure class value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure class value not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDate")));
				logger.info("Confirmation Page label return date available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return date not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnLocation")));
				logger.info("Confirmation Page label return location available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return location not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnCountry(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnCountry")));
				logger.info("Confirmation Page label return coutry available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return coutry not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnArrivalLocation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnArrivalLocation")));
				logger.info("Confirmation Page label return arrival location available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return arrival location not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnArrivalCountry(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnArrivalCountry")));
				logger.info("Confirmation Page label return arrival country available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return arrival country not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFlight(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFlight")));
				logger.info("Confirmation Page label return flight available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return flight not available",e);
			}
			return element;
		}
		
		public static WebElement label_redturnArrival(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_redturnArrival")));
				logger.info("Confirmation Page label return arrival available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return arrival not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnTime")));
				logger.info("Confirmation Page label return time available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return time not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnAirport(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnAirport")));
				logger.info("Confirmation Page label return airport available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return airport not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDuration(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDuration")));
				logger.info("Confirmation Page label return duration available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return duration not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnClass")));
				logger.info("Confirmation Page label return class available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return class not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFlightValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFlightValue")));
				logger.info("Confirmation Page label return flight value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return flight value not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnArrivalValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnArrivalValue")));
				logger.info("Confirmation Page label return arrival value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return arrival value not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnTimeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnTimeValue")));
				logger.info("Confirmation Page label return time value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return time value not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnAirportValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnAirportValue")));
				logger.info("Confirmation Page label return airport value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return airport value not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDurationValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDurationValue")));
				logger.info("Confirmation Page label return duration value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return duration value not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnClassValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnClassValue")));
				logger.info("Confirmation Page label return class value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return class value not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerType")));
				logger.info("Confirmation Page label passenger type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger type not available",e);
			}
			return element;
		}
		
		public static WebElement label_numberOfPax(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_numberOfPax")));
				logger.info("Confirmation Page label number of pax available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label number of pax not available",e);
			}
			return element;
		}
		
		public static WebElement label_departure(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departure")));
				logger.info("Confirmation Page label departure available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label departure not available",e);
			}
			return element;
		}
		
		public static WebElement label_return(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_return")));
				logger.info("Confirmation Page label return available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label return not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerTypeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerTypeValue")));
				logger.info("Confirmation Page label passsenger type value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passsenger type value not available",e);
			}
			return element;
		}
		
		public static WebElement label_numberOfPaxValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_numberOfPaxValue")));
				logger.info("Confirmation Page label number of pax available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label number of pax not available",e);
			}
			return element;
		}
		
		public static WebElement link_fareRulesButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_fareRulesButton")));
				logger.info("Confirmation Page link button fare rules available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page link button fare rules not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightPassengerInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightPassengerInfo")));
				logger.info("Confirmation Page label flight passenger info available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label flight passenger info not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoPassengerType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoPassengerType")));
				logger.info("Confirmation Page label passenger info passenger type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info passenger type not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoFName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoFName")));
				logger.info("Confirmation Page label passenger info first name available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info first name not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoLName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoLName")));
				logger.info("Confirmation Page label passenger info last name available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info last name not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoContact(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoContact")));
				logger.info("Confirmation Page label passenger info contact available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info contact not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoFFP(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoFFP")));
				logger.info("Confirmation Page label passenger info FFP available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info FFP not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoETicket(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoETicket")));
				logger.info("Confirmation Page label passenger info E Ticket available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info E Ticket not available",e);
			}
			return element;
		}
		
		public static WebElement label_PassengerInfoPassengerTypeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_PassengerInfoPassengerTypeValue")));
				logger.info("Confirmation Page label passenger info passenger type value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info passenger type value not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoFNameValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoFNameValue")));
				logger.info("Confirmation Page label passenger info first name value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info first name value not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoLNameValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoLNameValue")));
				logger.info("Confirmation Page label passenger info last name value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info last name value not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoContactValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoContactValue")));
				logger.info("Confirmation Page label passenger info contact value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info contact value not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerInfoValues(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerInfoValues")));
				logger.info("Confirmation Page label passenger info values available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label passenger info values not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelItinerary(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelItinerary")));
				logger.info("Confirmation Page label hotel itinerary available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel itinerary not available",e);
			}
			return element;
		}
		
		public static WebElement label_confirmationNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_confirmationNumber")));
				logger.info("Confirmation Page label confirmation number available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label confirmation number not available",e);
			}
			return element;
		}
		
		public static WebElement img_hotelImage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_hotelImage")));
				logger.info("Confirmation Page hotel image available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page hotel image not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelName")));
				logger.info("Confirmation Page label hotel name available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel name not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelAddress(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelAddress")));
				logger.info("Confirmation Page label hotel address available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel address not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelAddressValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelAddressValue")));
				logger.info("Confirmation Page label hotel address value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel address value not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckIn(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckIn")));
				logger.info("Confirmation Page label hotel check in available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel check in not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckInValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckInValue")));
				logger.info("Confirmation Page label hotel check in value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel check in value not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckOut(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckOut")));
				logger.info("Confirmation Page label hotel check out available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel check out not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckOutValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckOutValue")));
				logger.info("Confirmation Page label hotel check out value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel check out value not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelNights(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelNights")));
				logger.info("Confirmation Page label hotel nights available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel nights not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelNightsValues(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelNightsValues")));
				logger.info("Confirmation Page label hotel nights values available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel nights values not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRooms(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRooms")));
				logger.info("Confirmation Page label hotel rooms available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel rooms not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomsValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomsValue")));
				logger.info("Confirmation Page label hotel rooms value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel rooms value not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomNumber")));
				logger.info("Confirmation Page label hotel room number available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel room number not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomNumberValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomNumberValue")));
				logger.info("Confirmation Page label hotel room number value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel room number value not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomType")));
				logger.info("Confirmation Page label hotel room type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel room type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomTypeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomTypeValue")));
				logger.info("Confirmation Page label hotel room type value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel room type value not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelBedType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelBedType")));
				logger.info("Confirmation Page label hotel bed type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel bed type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelBedTypeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelBedTypeValue")));
				logger.info("Confirmation Page label hotel bed type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel bed type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelMealPlan(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelMealPlan")));
				logger.info("Confirmation Page label hotel meal plan available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel meal plan not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSupRoom(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSupRoom")));
				logger.info("Confirmation Page label hotel supplementary room available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel supplementary room not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSupChargeBasedOn(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSupChargeBasedOn")));
				logger.info("Confirmation Page label hotel supplementary charge based on available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel supplementary charge based on not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSupName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSupName")));
				logger.info("Confirmation Page label hotel supplementary name available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel supplementary name not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSupRoomType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSupRoomType")));
				logger.info("Confirmation Page label hotel supplementary room type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel supplementary room type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSupAvailableDates(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSupAvailableDates")));
				logger.info("Confirmation Page label hotel supplementary available dates available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label hotel supplementary available dates not available",e);
			}
			return element;
		}
		
		public static WebElement label_cancellationPolicy(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_cancellationPolicy")));
				logger.info("Confirmation Page label cancellation policy available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label cancellation policy dates not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyInfo")));
				logger.info("Confirmation Page label room occupancy info available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy info not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyRoomNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyRoomNumber")));
				logger.info("Confirmation Page label room occupancy room number available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy room number not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyType")));
				logger.info("Confirmation Page label room occupancy type available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy type not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyFirstName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyFirstName")));
				logger.info("Confirmation Page label room occupancy first name available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy first name not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyLastName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyLastName")));
				logger.info("Confirmation Page label room occupancy last name available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy last name not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyRoomNumberValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyRoomNumberValue")));
				logger.info("Confirmation Page label room occupancy room number value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy room number value not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyTypeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyTypeValue")));
				logger.info("Confirmation Page label room occupancy type value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy type value not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyFirstNameValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyFirstNameValue")));
				logger.info("Confirmation Page label room occupancy first name value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy first name value not available",e);
			}
			return element;
		}
		
		public static WebElement label_roomOccupancyLastNameValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_roomOccupancyLastNameValue")));
				logger.info("Confirmation Page label room occupancy last name value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label room occupancy last name value not available",e);
			}
			return element;
		}
		
		public static WebElement label_invoiceSummary(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_invoiceSummary")));
				logger.info("Confirmation Page label invoice summary available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label invoice summary not available",e);
			}
			return element;
		}
		
		public static WebElement label_invoiceCurrency(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_invoiceCurrency")));
				logger.info("Confirmation Page label invoice currancy available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label invoice currancy not available",e);
			}
			return element;
		}
		
		public static WebElement label_serviceAndOtherCharges(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_serviceAndOtherCharges")));
				logger.info("Confirmation Page label service and other charges available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label service and other charges not available",e);
			}
			return element;
		}
		
		public static WebElement label_serviceAndOtherChargesValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_serviceAndOtherChargesValue")));
				logger.info("Confirmation Page label service and other charges value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label service and other charges value not available",e);
			}
			return element;
		}
		
		public static WebElement label_adminFee(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adminFee")));
				logger.info("Confirmation Page label admin fee available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label admin fee not available",e);
			}
			return element;
		}
		
		public static WebElement label_adminFeeValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adminFeeValue")));
				logger.info("Confirmation Page label admin fee value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label admin fee value not available",e);
			}
			return element;
		}
		
		public static WebElement label_taxes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_taxes")));
				logger.info("Confirmation Page label taxes available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_taxesValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_taxesValue")));
				logger.info("Confirmation Page label taxes value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label taxes value not available",e);
			}
			return element;
		}
		
		public static WebElement label_foodAndBeverages(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_foodAndBeverages")));
				logger.info("Confirmation Page label food and beverages available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label food and beverages not available",e);
			}
			return element;
		}
		
		public static WebElement label_foodAndBeveragesValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_foodAndBeveragesValue")));
				logger.info("Confirmation Page label food and beverages value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label food and beverages value not available",e);
			}
			return element;
		}
		
		public static WebElement label_otherTaxes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_otherTaxes")));
				logger.info("Confirmation Page label other taxes available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label other taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_otherTaxesValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_otherTaxesValue")));
				logger.info("Confirmation Page label other taxes available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label other taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_overallValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_overallValue")));
				logger.info("Confirmation Page label overall value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label overall value not available",e);
			}
			return element;
		}
		
		public static WebElement label_overallValueValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_overallValueValue")));
				logger.info("Confirmation Page label overall value value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label overall value value not available",e);
			}
			return element;
		}
		
		public static WebElement label_loremNote(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_loremNote")));
				logger.info("Confirmation Page label lorem note available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label lorem note not available",e);
			}
			return element;
		}
		
		public static WebElement link_termsAndConditionsButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_termsAndConditionsButton")));
				logger.info("Confirmation Page link button terms and conditions available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page link button terms and conditions not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentInformation(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentInformation")));
				logger.info("Confirmation Page label payment information available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment information not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentMethod(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentMethod")));
				logger.info("Confirmation Page label payment method available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment payment method not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentMethodValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentMethodValue")));
				logger.info("Confirmation Page label payment method value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment payment method value not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentReference(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentReference")));
				logger.info("Confirmation Page label payment reference available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment reference not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentReferenceValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentReferenceValue")));
				logger.info("Confirmation Page label payment reference value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label payment reference value not available",e);
			}
			return element;
		}
		
		public static WebElement label_totalAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_totalAmount")));
				logger.info("Confirmation Page label total amount available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label total amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_totalAmountValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_totalAmountValue")));
				logger.info("Confirmation Page label total amount value available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page label total amount value not available",e);
			}
			return element;
		}
		
		public static WebElement button_printbutton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_printbutton")));
				logger.info("Confirmation Page print button available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page print button not available",e);
			}
			return element;
		}
		
		public static WebElement img_thankYou(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("ConfirmationPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_thankYou")));
				logger.info("Confirmation Page image thank you available");
			} catch (Exception e) {
				logger.fatal("Confirmation Page image thank you not available",e);
			}
			return element;
		}
	}
}
