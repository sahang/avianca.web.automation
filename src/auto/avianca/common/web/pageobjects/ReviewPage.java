package auto.avianca.common.web.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import auto.avianca.common.web.utilities.PG_Properties;
import auto.avianca.common.web.utilities.PhraseElement;

public class ReviewPage {
	
	public ReviewPage(){
		
	}
	
	public static class PackageReviewPage{
		public static WebElement img_aviancaLogo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_aviancaLogo")));
				logger.info("Package Review page image avianca logo available");
			} catch (Exception e) {
				logger.fatal("Package Review page image avianca logo not available",e);
			}
			return element;
		}
		
		public static WebElement link_homeButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("link_homeButton")));
				logger.info("Package Review page home link button available");
			} catch (Exception e) {
				logger.fatal("Package Review page home link button not available",e);
			}
			return element;
		}
		
		public static WebElement img_searchPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_searchPanel")));
				logger.info("Package Review page search panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page search panel image not available",e);
			}
			return element;
		}
		
		public static WebElement img_selectionPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_selectionPanel")));
				logger.info("Package Review page selection panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page selection panel image not available",e);
			}
			return element;
		}
		
		public static WebElement img_reviewPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_reviewPanel")));
				logger.info("Package Review page review panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page review panel image not available",e);
			}
			return element;
		}
		
		public static WebElement img_confirmPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("img_confirmPanel")));
				logger.info("Package Review page confirm panel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page confirm panel image not available",e);
			}
			return element;
		}
		
		public static WebElement button_changeFlightButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_changeFlightButton")));
				logger.info("Package Review page change flight button available");
			} catch (Exception e) {
				logger.fatal("Package Review page change flight button not available",e);
			}
			return element;
		}
		
		public static WebElement button_addHotelButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_addHotelButton")));
				logger.info("Package Review page add hotel button available");
			} catch (Exception e) {
				logger.fatal("Package Review page add hotel button not available",e);
			}
			return element;
		}
		
		public static WebElement button_removeHotelButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_removeHotelButton")));
				logger.info("Package Review page remove hotel button available");
			} catch (Exception e) {
				logger.fatal("Package Review page remove hotel button not available",e);
			}
			return element;
		}
		
		public static WebElement button_selectCarButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_selectCarButton")));
				logger.info("Package Review page select car button available");
			} catch (Exception e) {
				logger.fatal("Package Review page select car button not available",e);
			}
			return element;
		}
		
		public static WebElement button_selectActivityButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_selectActivityButton")));
				logger.info("Package Review page select activity button available");
			} catch (Exception e) {
				logger.fatal("Package Review page select activity button not available",e);
			}
			return element;
		}
		
		public static WebElement button_bookNowButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("button_bookNowButton")));
				logger.info("Package Review page book now button available");
			} catch (Exception e) {
				logger.fatal("Package Review page book now button not available",e);
			}
			return element;
		}
		
		public static WebElement label_totalAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_totalAmount")));
				logger.info("Package Review page label total amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label total amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightsection(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightsection")));
				logger.info("Package Review page label flight section available");
			} catch (Exception e) {
				logger.fatal("Package Review page label flight section not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDescription")));
				logger.info("Package Review page label depart description available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart description not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFlightNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFlightNumber")));
				logger.info("Package Review page label depart flight number available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart flight number not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDate")));
				logger.info("Package Review page label depart date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart date not available",e);
			}
			return element;
		}
		
		public static WebElement label_departTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departTime")));
				logger.info("Package Review page label depart time available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart time not available",e);
			}
			return element;
		}
		
		public static WebElement label_departAirport(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departAirport")));
				logger.info("Package Review page label depart airport available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart airport not available",e);
			}
			return element;
		}
		
		public static WebElement label_departDuration(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departDuration")));
				logger.info("Package Review page label depart duration available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart duration not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFlightClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFlightClass")));
				logger.info("Package Review page label depart flight class available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart flight class not available",e);
			}
			return element;
		}
		
		public static WebElement label_departInfoPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departInfoPanel")));
				logger.info("Package Review page label depart info panel available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart info panel not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDescription(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDescription")));
				logger.info("Package Review page label return description available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return description not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFlightNumber(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFlightNumber")));
				logger.info("Package Review page label return flight number available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return flight number not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDate")));
				logger.info("Package Review page label return date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return date not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnTime(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnTime")));
				logger.info("Package Review page label return time available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return time not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnAirport(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnAirport")));
				logger.info("Package Review page label return airport available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return airport not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnDuration(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnDuration")));
				logger.info("Package Review page label return duration available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return duration not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFlightClass(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFlightClass")));
				logger.info("Package Review page label return flight class available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return flight class not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnInfoPanel(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnInfoPanel")));
				logger.info("Package Review page label return info panel available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return info not available",e);
			}
			return element;
		}
		
		public static WebElement label_passengerType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_passengerType")));
				logger.info("Package Review page label passenger type available");
			} catch (Exception e) {
				logger.fatal("Package Review page label passenger type not available",e);
			}
			return element;
		}
		
		public static WebElement label_noOfPax(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_noOfPax")));
				logger.info("Package Review page label number of pax available");
			} catch (Exception e) {
				logger.fatal("Package Review page label number of pax not available",e);
			}
			return element;
		}
		
		public static WebElement label_departFareRules(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_departFareRules")));
				logger.info("Package Review page label depart fare rules available");
			} catch (Exception e) {
				logger.fatal("Package Review page label depart fare rules not available",e);
			}
			return element;
		}
		
		public static WebElement label_returnFareRules(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_returnFareRules")));
				logger.info("Package Review page label return fare rules available");
			} catch (Exception e) {
				logger.fatal("Package Review page label return fare rules not available",e);
			}
			return element;
		}
		
		public static WebElement label_flightPassengerInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_flightPassengerInfo")));
				logger.info("Package Review page label flight passenger info available");
			} catch (Exception e) {
				logger.fatal("Package Review page label flight passenger info not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelSection(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelSection")));
				logger.info("Package Review page label hotel section available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel section not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelImage(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelImage")));
				logger.info("Package Review page label hotel image available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel image not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelName")));
				logger.info("Package Review page label hotel name available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel name not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelAddress(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelAddress")));
				logger.info("Package Review page label hotel address available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel address not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckInDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckInDate")));
				logger.info("Package Review page label hotel check in date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel check in date not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelCheckOutDate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelCheckOutDate")));
				logger.info("Package Review page label hotel check out date available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel check out date not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelNightCount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelNightCount")));
				logger.info("Package Review page label hotel night count available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel night count not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomCount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomCount")));
				logger.info("Package Review page label hotel room count available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room count not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRemoveButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRemoveButton")));
				logger.info("Package Review page label hotel remove button available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel remove button not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoom(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoom")));
				logger.info("Package Review page label hotel room available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomType")));
				logger.info("Package Review page label hotel room type available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelBedType(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelBedType")));
				logger.info("Package Review page label hotel bed type available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel bed type not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelMealPlan(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelMealPlan")));
				logger.info("Package Review page label hotel meal plan available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel meal plan not available",e);
			}
			return element;
		}
		
		public static WebElement label_hotelRoomInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_hotelRoomInfo")));
				logger.info("Package Review page label hotel room info available");
			} catch (Exception e) {
				logger.fatal("Package Review page label hotel room info not available",e);
			}
			return element;
		}
		
		public static WebElement label_buttonContinueButton(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_buttonContinueButton")));
				logger.info("Package Review page button continue available");
			} catch (Exception e) {
				logger.fatal("Package Review page button continue not available",e);
			}
			return element;
		}
		
		public static WebElement label_paymentDetails(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_paymentDetails")));
				logger.info("Package Review page label payment details available");
			} catch (Exception e) {
				logger.fatal("Package Review page label payment details not available",e);
			}
			return element;
		}
		
		public static WebElement label_priceUnit(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceUnit")));
				logger.info("Package Review page label price unit available");
			} catch (Exception e) {
				logger.fatal("Package Review page label price unit not available",e);
			}
			return element;
		}
		
		public static WebElement label_serviceValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_serviceValue")));
				logger.info("Package Review page label service value available");
			} catch (Exception e) {
				logger.fatal("Package Review page label service value not available",e);
			}
			return element;
		}
		
		public static WebElement label_serviceAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_serviceAmount")));
				logger.info("Package Review page label service amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label service amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_adminFee(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adminFee")));
				logger.info("Package Review page label admin fee available");
			} catch (Exception e) {
				logger.fatal("Package Review page label admin fee not available",e);
			}
			return element;
		}
		
		public static WebElement label_adminFeeAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_adminFeeAmount")));
				logger.info("Package Review page label admin fee amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label admin fee amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_taxes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_taxes")));
				logger.info("Package Review page label taxes available");
			} catch (Exception e) {
				logger.fatal("Package Review page label taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_taxesAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_taxesAmount")));
				logger.info("Package Review page label taxes amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label taxes amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_foodAndBeverages(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_foodAndBeverages")));
				logger.info("Package Review page label food and beverages available");
			} catch (Exception e) {
				logger.fatal("Package Review page label food and beverages not available",e);
			}
			return element;
		}
		
		public static WebElement label_foodAndBeveragesAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_foodAndBeveragesAmount")));
				logger.info("Package Review page label food and beverages amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label food and beverages amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_otherTaxes(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_otherTaxes")));
				logger.info("Package Review page label other taxes available");
			} catch (Exception e) {
				logger.fatal("Package Review page label other taxes not available",e);
			}
			return element;
		}
		
		public static WebElement label_otherTaxesAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_otherTaxesAmount")));
				logger.info("Package Review page label other taxes amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label other taxes amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_overallValue(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_overallValue")));
				logger.info("Package Review page label overall value available");
			} catch (Exception e) {
				logger.fatal("Package Review page label overall value not available",e);
			}
			return element;
		}
		
		public static WebElement label_overallValueAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_overallValueAmount")));
				logger.info("Package Review page label overall value amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label overall value amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_priceByPerson(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceByPerson")));
				logger.info("Package Review page label price by person available");
			} catch (Exception e) {
				logger.fatal("Package Review page label price by person not available",e);
			}
			return element;
		}
		
		public static WebElement label_priceByPersonAmount(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_priceByPersonAmount")));
				logger.info("Package Review page label price by person amount available");
			} catch (Exception e) {
				logger.fatal("Package Review page label price by person amount not available",e);
			}
			return element;
		}
		
		public static WebElement label_spanishText(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_spanishText")));
				logger.info("Package Review page label spanish text available");
			} catch (Exception e) {
				logger.fatal("Package Review page label spanish text not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryHeader(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryHeader")));
				logger.info("Package Review page label supplementary header available");
			} catch (Exception e) {
				logger.fatal("Package Review page label spanish text not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryName(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryName")));
				logger.info("Package Review page label supplementary name available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary name not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryRate(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryRate")));
				logger.info("Package Review page label supplementary rate available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary rate not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementarySelection(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementarySelection")));
				logger.info("Package Review page label supplementary selection available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary selection not available",e);
			}
			return element;
		}
		
		public static WebElement label_supplementaryInfo(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_supplementaryInfo")));
				logger.info("Package Review page label supplementary info available");
			} catch (Exception e) {
				logger.fatal("Package Review page label supplementary info not available",e);
			}
			return element;
		}
		
		public static WebElement label_cancellationPolicy(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_cancellationPolicy")));
				logger.info("Package Review page label cancellation policy available");
			} catch (Exception e) {
				logger.fatal("Package Review page label cancellation policy not available",e);
			}
			return element;
		}
		
		public static WebElement check_termsAndConditionsBox(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("check_termsAndConditionsBox")));
				logger.info("Package Review page terms and conditions check box available");
			} catch (Exception e) {
				logger.fatal("Package Review page terms and conditions check box not available",e);
			}
			return element;
		}
		
		public static WebElement label_termsAndCondition(WebDriver driver){
			WebElement element = null;
			Logger logger = Logger.getLogger("PackageReviewPage");
			try {
				element = driver.findElement(PhraseElement.getByType(PG_Properties.getProperty("label_termsAndCondition")));
				logger.info("Package Review page label terms and conditions available");
			} catch (Exception e) {
				logger.fatal("Package Review page label terms and conditions not available",e);
			}
			return element;
		}
	}
}
