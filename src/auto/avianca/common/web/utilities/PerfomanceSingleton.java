package auto.avianca.common.web.utilities;

public class PerfomanceSingleton {
		  // Private constructor prevents instantiation from other classes
	
	private double TimeTakenToLoadBookingEngine                   = 1L;
	private double TimeTakenToDisplayResults                      = 1L;
	private double TimeTakenToAddToCart                           = 1L;
	private double TimeTakenToConfirmation                        = 1L;
     
		  private PerfomanceSingleton() {}
		 
		  /**
		   * SingletonHolder is loaded on the first execution of Singleton.getInstance() 
		   * or the first access to SingletonHolder.INSTANCE, not before.
		   */
		  private static class SingletonHolder { 
		    private static final PerfomanceSingleton INSTANCE = new PerfomanceSingleton();
		  }

		  public static PerfomanceSingleton getInstance() {
		    return SingletonHolder.INSTANCE;
		  }

		public double getTimeTakenToLoadBookingEngine() {
			return TimeTakenToLoadBookingEngine;
		}

		public void setTimeTakenToLoadBookingEngine(double timeTakenToLoadBookingEngine) {
			TimeTakenToLoadBookingEngine = timeTakenToLoadBookingEngine;
		}

		public double getTimeTakenToDisplayResults() {
			return TimeTakenToDisplayResults;
		}

		public void setTimeTakenToDisplayResults(double timeTakenToDisplayResults) {
			TimeTakenToDisplayResults = timeTakenToDisplayResults;
		}

		public double getTimeTakenToAddToCart() {
			return TimeTakenToAddToCart;
		}

		public void setTimeTakenToAddToCart(double timeTakenToAddToCart) {
			TimeTakenToAddToCart = timeTakenToAddToCart;
		}

		public double getTimeTakenToConfirmation() {
			return TimeTakenToConfirmation;
		}

		public void setTimeTakenToConfirmation(double timeTakenToConfirmation) {
			TimeTakenToConfirmation = timeTakenToConfirmation;
		}
		  
		 
		}

