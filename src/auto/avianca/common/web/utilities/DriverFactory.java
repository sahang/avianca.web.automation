package auto.avianca.common.web.utilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFactory {
	
private Logger logger = null;	
public DriverFactory(){
	logger = Logger.getLogger(this.getClass());
}

			
public WebDriver createDriver(HashMap<String, String>   propertyMap) throws MalformedURLException{
	
    WebDriver driver ;
	logger.info("====== Driver initialization started ========");
	String Type = propertyMap.get("BinaryType").trim();

	if (Type.equalsIgnoreCase("FirefoxBin")) {
		logger.info("FirefoxBin type selected ---> No profile");
		FirefoxProfile prof = new FirefoxProfile();
		FirefoxBinary bin = new FirefoxBinary();
		driver = new FirefoxDriver(bin, prof);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		return driver;
	}
	if (Type.equalsIgnoreCase("Phantom")) {
		logger.info("Phantom Driver type selected ---> No profile");
		String ExecutablePath = propertyMap.get("phantomjs.executable.path");
		logger.info("PhantomJs Path --> " + ExecutablePath);
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, ExecutablePath);
		cap.setCapability("TakeScreenshot", true);
		driver = new PhantomJSDriver(cap);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		return driver;

	}
	if (Type.equalsIgnoreCase("Firefoxpro")) {

		File profile = new File(propertyMap.get("firefox.profile.path").trim());
		logger.info("Firefox Profile type selected --->Profile --->" + profile.getAbsolutePath());
		FirefoxProfile prof = new FirefoxProfile(profile);
		driver = new FirefoxDriver(prof);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		return driver;

	} 
	if (Type.equalsIgnoreCase("RemoteDriver")) {
		
		DesiredCapabilities caps             = new DesiredCapabilities();
		driver = new RemoteWebDriver(new URL(propertyMap.get("Remote.hub.url")), caps);
		return driver;
	}else {
		FirefoxProfile prof = new FirefoxProfile();
		logger.info("Driver Type Not Selected");
		driver = new FirefoxDriver(prof);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		return driver;
	}



	
}

}
