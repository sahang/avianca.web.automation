package auto.avianca.common.web.utilities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScreenshotPathSingleton {
		  // Private constructor prevents instantiation from other classes
	
	private  String                RootDir                                = "Screenshots/";
	private  String                BasicCommonPath                        = "Screenshot/";
	private  String                BasicFailedPath                        = "Screenshot/";
	private  String                ScenarioCommonPath                     = "Screenshot/";
	private  String                ScenarioFailedPath                     = "Screenshot/";

     
		  private ScreenshotPathSingleton() {
			  Calendar cal  = Calendar.getInstance();
			  String   Date = new SimpleDateFormat("dd-MM-yyy").format(cal.getTime());
			 
			  RootDir          = "Screenshots/"+Date;
			  BasicCommonPath  = RootDir+"/General/Common";
			  BasicFailedPath  = RootDir+"/General/Failed";
				
			    File Basic       = new File(BasicCommonPath);
			    File BasicFailed = new File(BasicFailedPath);

			    createDirs(Basic);
			    createDirs(BasicFailed);
		  }
		 
		  /**
		   * SingletonHolder is loaded on the first execution of Singleton.getInstance() 
		   * or the first access to SingletonHolder.INSTANCE, not before.
		   */
		  private static class SingletonHolder { 
		    private static final ScreenshotPathSingleton INSTANCE = new ScreenshotPathSingleton();
		  }

		  public static ScreenshotPathSingleton getInstance() {
		    return SingletonHolder.INSTANCE;
		  }
		  
		  private void createDirs(File file){
			  if (!file.exists()) {
					//logger.info("Screenshot path not available - Creating directories..");

					try {
						file.mkdirs();
						//logger.info("Directory created succcessfully!!");
					} catch (Exception e) {
						//logger.fatal("Directory creation failed " + e.toString());
					}
				}
			  
		  }

		public String getBasicCommonPath() {
			return BasicCommonPath;
		}

		public String getBasicFailedPath() {
			return BasicFailedPath;
		}


		public String getScenarioCommonPath() {
			return ScenarioCommonPath;
		}

		
		public String getScenarioFailedPath() {
			return ScenarioFailedPath;
		}

		public void setScenarioPaths(String scenario) {
			ScenarioCommonPath   = RootDir+"/"+scenario+"/Common";
			ScenarioFailedPath   = RootDir+"/"+scenario+"/Failed";
			
			createDirs(new File(ScenarioCommonPath));
			createDirs(new File(ScenarioFailedPath));
		}
          
		  
		 
		}

