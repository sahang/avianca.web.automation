package auto.avianca.common.web.utilities;
/**
 * @author Dulan
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;




import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;


public class ExcelReader { 
	
	

	

	public ArrayList<Map<Integer,String>> init(String filePath) {
    

		FileInputStream fs = null;
		ArrayList<Map<Integer,String>> returnlist=null;
		try {
		
			
			fs = new FileInputStream(new File(filePath));
	        returnlist = contentReading(fs);
		} catch (IOException e) {
			
			
		} catch (Exception e) {
		  
		
			
		} finally {
			try {
				fs.close();
			} catch (IOException e) {
			System.out.println("Cannot Find Specified file....!!!");
			//e.printStackTrace();
			}
		}
		return returnlist;
	}



	public  ArrayList<Map<Integer, String>>  contentReading(InputStream fileInputStream) {
		
		
		ArrayList<Map<Integer, String>> list = null;
		WorkbookSettings ws = null;
		Workbook workbook = null;
	    int totalSheet = 0;
       
		try {
		
			ws = new WorkbookSettings();
			ws.setLocale(new Locale("en", "EN"));
			workbook = Workbook.getWorkbook(fileInputStream, ws);

			totalSheet = workbook.getNumberOfSheets();
			
		
			list = new ArrayList<Map<Integer,String>>(totalSheet);
            
			for (int sheet = 0 ; sheet<totalSheet ; sheet++) {
				
			    list.add(getSheetData(workbook.getSheet(sheet) ));	
		
			}
			
		
			workbook.close();
		} catch (IOException e) {
			//e.printStackTrace();
		} catch (BiffException e) {
			//e.printStackTrace();
		}
		
		return list;
	}

	public static void main(String[] args) {
		try {
			System.out.println("begin");
			ExcelReader xlReader = new ExcelReader();
			ArrayList<Map<Integer, String>> list1 = xlReader.init("C:/NewDevelopment/V3-Hotel/Excel/V3.xls");
			System.out.println(list1);
			
			System.out.println("end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Map<Integer, String> getSheetData(Sheet s)
	{
		int rawCount = s.getRows();
		
		int columnCount = s.getColumns();
		Map<Integer, String> map = new HashMap<Integer, String>();
		for(int count=1 ; count<rawCount ; count++)
		{
			Cell[] rawdata = s.getRow(count);
			
			String CellContent = rawdata[0].getContents();
			for(int index = 1 ; index<columnCount ; index++)
			{
				CellContent = CellContent.concat(",").concat(rawdata[index].getContents());
			}
			
			map.put(count-1, CellContent);
			
		}
		
	  return map;
	}
	
}