package auto.avianca.common.web.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.HashMap;
import java.util.Map;

public class PropertyReader {

	public static void main(String[] args) throws IOException{
		
		PropertyReader reader = new PropertyReader();
		System.out.println(reader.readProperty("Avianca.web.url"));
	}
	
	public static String readProperty(String Property)throws IOException {
		
		Properties prop = new Properties();
		FileInputStream stream = new FileInputStream(new File("pageElements.properties"));
		prop.load(stream);
		return prop.getProperty(Property);
	}
	
	public static HashMap<String,String> getPropertyMap(String path) throws IOException {
		
		Properties prop = new Properties();
		FileInputStream stream = new FileInputStream(new File(path));
		prop.load(stream);
		HashMap<String,String> propertyMap = new HashMap<String, String>((Map<? extends String, ? extends String>) prop.entrySet());
		return propertyMap;
	}
	
	
	public static HashMap<String,String> getPropertyMap() throws IOException {
		
		Properties prop = new Properties();
		FileInputStream stream = new FileInputStream(new File("pageElements.properties"));
		prop.load(stream);
		HashMap<String,String> propertyMap = new HashMap<String, String>((Map<? extends String, ? extends String>) prop.entrySet());
		return propertyMap;
	}

}
