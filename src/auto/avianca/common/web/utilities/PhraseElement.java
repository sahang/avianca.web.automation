package auto.avianca.common.web.utilities;

import org.openqa.selenium.By;

public class PhraseElement {
	
	public static By selectedByType = null;
	public PhraseElement(){
		
	}
	
	public static By getByType(String type)
	{
		String LocatorType = type.split(":")[0];
		String LocatorPath = type.split(":")[1];
		
		switch (LocatorType) {
		case "id":
			selectedByType = By.id(LocatorPath);
			break;
			
		case "name":
			selectedByType = By.name(LocatorPath);
			break;
		
		case "xpath":
			selectedByType=By.xpath(LocatorPath);
			break;
			
		case "class":
			selectedByType=By.className(LocatorPath);
			break;
			
		case "tagname":
			selectedByType=By.tagName(LocatorPath);
			break;
			
		case "link":
			selectedByType=By.linkText(LocatorPath);
			break;
			
		case "csslocator":
			selectedByType=By.cssSelector(LocatorPath);
			break;
			
		case "partialLinkText":
			selectedByType=By.partialLinkText(LocatorPath);
			break;

		default:
			break;
			
		}
		return selectedByType;
	}

}
