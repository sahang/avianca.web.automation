package auto.avianca.common.web.utilities;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class PG_Properties {

	private static final String RELATIVE_URL = "pageElements.properties";
	private static FileReader reader;
	
	public static void main(String[] args) {
		
		System.out.println(getProperty("Proxy.port"));
	}
	
	public static String getProperty(String key) {
		
		Properties prop = new Properties();
		
		try {
			reader = new FileReader(new File(RELATIVE_URL));
			prop.load(reader);
		} 
		catch (Exception e) {
			System.out.println("name"+e.toString());
		}
		
		return prop.getProperty(key);
	}
}
