package auto.avianca.common.web.runner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import auto.avianca.common.loaders.VacationDataLoader;
import auto.avianca.common.web.enumtypes.ValidationType;
import auto.avianca.common.web.utilities.DriverFactory;
import auto.avianca.common.web.utilities.ExcelReader;
import auto.avianca.common.web.utilities.PropertyReader;
import auto.avianca.holidays.web.drivers.HomePageDriver;
import auto.avianca.holidays.web.pojo.PackageSearchScenario;

public class PackageRunner {

	private WebDriver                              driver;
	private HashMap<String, String>                PropertySet;
	private Map<String, PackageSearchScenario>     SearchList = null;
	private ArrayList<Map<Integer, String>>        SheeetList;
    private StringBuffer                           PrintWriter;
	private int                                    ScenarioCount;
	public static int                              TestCaseCount = 1;
	private Map<Integer, String>                   SearchScenarioMap;
	private Logger                                 logger;

	@Before
	public void setUp() throws IOException {
		logger = Logger.getLogger(this.getClass());
		DOMConfigurator.configure("log4j.xml");
		
		SearchList                       = new HashMap<String, PackageSearchScenario>();
		VacationDataLoader ScenarioLoder = new VacationDataLoader();
		ExcelReader readExcel            = new ExcelReader();
		PropertySet                      = PropertyReader.getPropertyMap();
		PrintWriter                      = new StringBuffer();
		DriverFactory   factory          = new DriverFactory();
		

		ScenarioCount = 0;

		try {
			logger.info("Initializing Excel WorkBooks -");
			logger.info("WorkBooks Paths -Search Excel---> " + PropertySet.get("Holidays.Scenario.Excel.Path= "));
			SheeetList = readExcel.init(PropertySet.get("Holidays.Scenario.Excel.Path"));

		} catch (Exception e) {
			logger.fatal("Error when initializing the Excel WorkBooks -", e);
		}

		SearchScenarioMap = SheeetList.get(0);

		// Loading Search Details
		try {
			SearchList = ScenarioLoder.load_Pkg_Scenarios(SearchScenarioMap);
			logger.info("Package Seacrch List Loaded Successfully");
			logger.debug(SearchList);
		} catch (Exception e) {
			logger.fatal("Package Seacrch List Not Loaded Successfully");
			logger.fatal(e.toString());
		}
		
		//Driver Initialization 
		
		try {
			driver = factory.createDriver(PropertySet);
			logger.info("Driver Initialized Successfully !!!!");
		} catch (Exception e) {
			logger.fatal("Driver Initialization Failed",e);
	
		}

	}

	@Test
	public void runTest() throws IOException, InterruptedException {
		
	//Starting the flow
	//TODO: Report Header or Excel Header
	
	if(!(SearchList == null || (SearchList.size() == 0))){
		
		Iterator<Map.Entry<String, PackageSearchScenario>>  itr = SearchList.entrySet().iterator();
		
        while (itr.hasNext()) {
			Map.Entry<java.lang.String, auto.avianca.holidays.web.pojo.PackageSearchScenario> entry = (Map.Entry<java.lang.String, auto.avianca.holidays.web.pojo.PackageSearchScenario>) itr.next();
			String Scenario                       = entry.getKey();
			PackageSearchScenario   CurrentSearch = entry.getValue();
			
			logger.info("Starting Scenario ----->" + Scenario);
			
			if(CurrentSearch.getTestType() == ValidationType.RESERVATION){
				HomePageDriver home_driver = new HomePageDriver();
				
				if(home_driver.loadHomePage(driver, PropertySet.get("Portal.Url"))){
					home_driver.selectLanguage(driver, CurrentSearch.getLanguage());
					home_driver.doPackageSearch(driver, CurrentSearch);
				}else{
					
				}
				
			}
			
		}
	
	}else{
		logger.debug("SearchList is empty test is aborted!!!");
	}
		
		
	}

	@After
	public void tearDown() throws IOException {

		driver.quit();
	}

}
