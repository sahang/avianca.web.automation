package auto.avianca.common.loaders;



import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import auto.avianca.common.web.enumtypes.DepartureType;
import auto.avianca.common.web.enumtypes.ReturnType;
import auto.avianca.common.web.enumtypes.SeatClass;
import auto.avianca.holidays.web.pojo.PackageSearchScenario;
//import Supplier.setup.Supplier;

public class VacationDataLoader {
	
org.apache.log4j.Logger dataLogger = null;
public VacationDataLoader()
{
	dataLogger = org.apache.log4j.Logger.getLogger(this.getClass());
}


public TreeMap<String,PackageSearchScenario> load_Pkg_Scenarios(Map<Integer, String> map)
{
    TreeMap<String, PackageSearchScenario>  returnmap = new TreeMap<>();
	Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
   
	while(it.hasNext())
	{
		PackageSearchScenario Scenario = new PackageSearchScenario();
		String[] AllValues	           = it.next().getValue().split(",");
		
		Scenario.setLanguage(AllValues[1]);
		Scenario.setDepartureDestination(AllValues[2]);
		Scenario.setArrivalDestination(AllValues[3]);
		Scenario.setAllCities(AllValues[4]);
		Scenario.setAllDestinations(AllValues[5]);
		Scenario.setDepartureDate(AllValues[6]);
		Scenario.setDepartureTimeType(DepartureType.getDepartureType(AllValues[7]));
		Scenario.setArrivalDestination(AllValues[8]);
		Scenario.setReturnTimeType(ReturnType.getReturnType(AllValues[9]));
		Scenario.setRoomCount(AllValues[10]);
		Scenario.setAdultCount(AllValues[11]);
		Scenario.setChildCount(AllValues[12]);
		Scenario.setChildAge(AllValues[13]);
		Scenario.setInfantCount(AllValues[14]);
		Scenario.setSeatClassType(SeatClass.getSeatClass(AllValues[15]));
		Scenario.setPromoCode(AllValues[16]);
		Scenario.setDirectFirst(AllValues[17]);
		Scenario.setPartialStayInHotel(AllValues[18]);
		Scenario.setPartialStayCheckInDate(AllValues[19]);
		Scenario.setPartialStayCheckOutDate(AllValues[20]);
		Scenario.setHotelInOtherCity(AllValues[21]);
		Scenario.setHotelInOtherCityName(AllValues[22]);

		
		returnmap.put(AllValues[0], Scenario);
	}
		return returnmap;
}
}
