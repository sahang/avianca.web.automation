package auto.avianca.holidays.web.drivers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import auto.avianca.common.web.enumtypes.PackageType;
import auto.avianca.common.web.pageobjects.HomePage;
import auto.avianca.common.web.pageobjects.ResultsPage;
import auto.avianca.holidays.web.pojo.ExpectedCarBookingEngine;
import auto.avianca.holidays.web.pojo.ExpectedHolidayBookingEngine;
import auto.avianca.holidays.web.pojo.ExpectedHotelBookingEngine;
import auto.avianca.holidays.web.pojo.PackageSearchScenario;
import auto.avianca.holidays.web.pojo.HotelSearchScenario;
import auto.avianca.holidays.web.pojo.CarSearchScenario;

public class ResultsPageDriver {
	
	public boolean flightResultsPage(WebDriver driver, PackageSearchScenario search){
		try {
			
			ResultsPage.FlightResultsPage.button_continueButton(driver).click();
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
