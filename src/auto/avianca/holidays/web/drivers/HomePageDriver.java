package auto.avianca.holidays.web.drivers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import auto.avianca.common.web.enumtypes.PackageType;
import auto.avianca.common.web.pageobjects.HomePage;
import auto.avianca.common.web.utilities.PG_Properties;
import auto.avianca.common.web.utilities.PhraseElement;
import auto.avianca.holidays.web.pojo.ExpectedCarBookingEngine;
import auto.avianca.holidays.web.pojo.ExpectedHolidayBookingEngine;
import auto.avianca.holidays.web.pojo.ExpectedHotelBookingEngine;
import auto.avianca.holidays.web.pojo.PackageSearchScenario;
import auto.avianca.holidays.web.pojo.HotelSearchScenario;
import auto.avianca.holidays.web.pojo.CarSearchScenario;

public class HomePageDriver {
	
//Booking Engine Selections	
	
	
public boolean loadHomePage(WebDriver driver,String URL){
	try {
	    driver.get(URL);
	    WebDriverWait wait = new WebDriverWait(driver, 120);
	    wait.until(ExpectedConditions.presenceOfElementLocated(PhraseElement.getByType(PG_Properties.getProperty("drop_languageBox"))));
	    return true;
	} catch (Exception e) {
		return false;
	}
}

public boolean selectLanguage(WebDriver driver,String Language){
	try {
		Select select = new Select(HomePage.BookingEngine.drop_languageBox(driver));
		select.selectByVisibleText(Language);
		return true;
	} catch (Exception e) {
		return false;
	}
}
	
//---------------------------------------------------------------------Package Search-----------------------------------------------------------------------------------

public boolean doPackageSearch(WebDriver driver, PackageSearchScenario search){
	try {
		
	//Selecting the package type
		if(search.getHolidayPackageType() == PackageType.FLIGHTHOTEL)
		HomePage.PackageScreen.radio_flightHotelButton(driver).click();
		
		else if(search.getHolidayPackageType() == PackageType.FLIGHTHOTELCAR)
		HomePage.PackageScreen.radio_flightHotelCarButton(driver).click();
		
		else if(search.getHolidayPackageType() == PackageType.FLIGHTHOTELACTIVITY)
		HomePage.PackageScreen.radio_flightHotelActivitiesButton(driver).click();
		
	//Entering the depart city and arrival city
		HomePage.PackageScreen.text_fromField(driver).sendKeys(search.getDepartureDestination());
		HomePage.PackageScreen.text_toField(driver).sendKeys(search.getArrivalDestination());
		
	//All cities and All destinations
		HomePage.PackageScreen.button_allCitiesButton(driver).click();
		HomePage.PackageScreen.button_allDestinationsButton(driver).click();
		
	//Selecting Departure Date
		HomePage.PackageScreen.dtp_departDate(driver).click();
		
	//Selecting Departure Time
		
	//Selecting Return Date
		HomePage.PackageScreen.dtp_returnDate(driver).click();
		
	//Selecting Return Time
		
	//Selecting Room count
		
	//Selecting Room Occupancy
		
	//Selecting Seat Class
		
	//Selecting Promotion code
		HomePage.PackageScreen.text_PromotionCode(driver).sendKeys(search.getPromoCode());
		
	//Selecting direct first check box
		HomePage.PackageScreen.check_directFirstBox(driver).click();
		
	//Selecting partial stay check box
		HomePage.PackageScreen.check_partialStayBox(driver).click();
		
	//Selecting partial stay check in and check out dates
		HomePage.PackageScreen.dtp_partialSatyCheckInDate(driver).click();
		HomePage.PackageScreen.dtp_partialSatyCheckOutDate(driver).click();
		
	//Selecting hotel in other city check box
		HomePage.PackageScreen.check_hotelOtherCityBox(driver).click();
		
	//Entering hotel in other city text filed
		HomePage.PackageScreen.text_partialStayCityField(driver).sendKeys(search.getHotelInOtherCity());
		
	//Search button
		HomePage.PackageScreen.button_searchButton(driver).click();
		
		return true;
	} catch (Exception e) {
		return false;
	}
}

//Selecting Package Departure Time
public boolean selectPackageDepartTime(WebDriver driver,String DepartTime){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_departTimeBox(driver));
		select.selectByVisibleText(DepartTime);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Package Return Time
public boolean selectPackageReturnTime(WebDriver driver,String ReturnTime){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_returnTimeBox(driver));
		select.selectByVisibleText(ReturnTime);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Package Room count
public boolean selectPackageRoomCount(WebDriver driver,String RoomCount){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_noOfRoomsBox(driver));
		select.selectByVisibleText(RoomCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Adult Count
public boolean selectPackageAdultCount(WebDriver driver,String AdultCount){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_noOfAdultsBox(driver));
		select.selectByVisibleText(AdultCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Child Count
public boolean selectPackageChildCount(WebDriver driver,String ChildCount){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_noOfChildrenBox(driver));
		select.selectByVisibleText(ChildCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Infant Count
public boolean selectPackageInfantCount(WebDriver driver,String InfantCount){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_noOfChildrenBox(driver));
		select.selectByVisibleText(InfantCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Select Seat Class
public boolean selectPackageSeatClass(WebDriver driver,String SeatClass){
	try {
		Select select = new Select(HomePage.PackageScreen.drop_seatClassBox(driver));
		select.selectByVisibleText(SeatClass);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//>>>>>>>>>>>>>>>>>>>To Check Package elements>>>>>>>>>>>>>>>>>>>>>>>>>>
public void checkPackagesBookingEngine(WebDriver driver){
	
	ExpectedHolidayBookingEngine  HolidayBookingEngine = new ExpectedHolidayBookingEngine();
	
//>>>To get options in Language drop down>>>
	HolidayBookingEngine.setDropLanguage(HomePage.BookingEngine.drop_languageBox(driver).isDisplayed());
	
	Select selectLanguage = new Select(HomePage.BookingEngine.drop_languageBox(driver));
	ArrayList<WebElement> LanguageOptionList = new ArrayList<WebElement>(selectLanguage.getOptions());
	ArrayList<String> StringLanguageOptionList = new ArrayList<String>(LanguageOptionList.size());
	
	for (Iterator iterator = LanguageOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringLanguageOptionList.add(element.getText());
	}
	
	HolidayBookingEngine.setLanguageOptionsList(StringLanguageOptionList);
	HolidayBookingEngine.setDefaultLanguage(selectLanguage.getFirstSelectedOption().getText());
	
	
//>>>To get options in Departure time drop down>>>
	HolidayBookingEngine.setDropDepartTime(HomePage.PackageScreen.drop_departTimeBox(driver).isDisplayed());
	
	Select selectDepartTime = new Select(HomePage.PackageScreen.drop_departTimeBox(driver));
	ArrayList<WebElement>  DepartTimeOptionList = new ArrayList<WebElement>(selectDepartTime.getOptions());
	ArrayList<String>      StringDepartTimeOptionList = new ArrayList<String>(DepartTimeOptionList.size());
	
	for (Iterator iterator = DepartTimeOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringDepartTimeOptionList.add(element.getText());	
	}
	
//To check options in drop down
	HolidayBookingEngine.setDepartureTimeOptionsList(StringDepartTimeOptionList);
	
//To get first option in the drop down
	HolidayBookingEngine.setDefaultDepartureTime(selectDepartTime.getFirstSelectedOption().getText());
	
	
//>>>To get options in Return Time drop down>>>
	HolidayBookingEngine.setDropReturnTime(HomePage.PackageScreen.drop_returnTimeBox(driver).isDisplayed());
	
	Select selectReturnTime = new Select(HomePage.PackageScreen.drop_returnTimeBox(driver));
	ArrayList<WebElement> ReturnTimeOptionList = new ArrayList<WebElement>(selectReturnTime.getOptions());
	ArrayList<String> StringReturnTimeOptionList = new ArrayList<String>(ReturnTimeOptionList.size());
	
	for (Iterator iterator = ReturnTimeOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringReturnTimeOptionList.add(element.getText());	
	}
	
	HolidayBookingEngine.setReturnTimeOptionsList(StringReturnTimeOptionList);
	HolidayBookingEngine.setDefaultReturnTime(selectReturnTime.getFirstSelectedOption().getText());
	
	
//>>>To get options in Room count drop down>>>
	HolidayBookingEngine.setDropRoomCount(HomePage.PackageScreen.drop_noOfRoomsBox(driver).isDisplayed());
		
	Select selectRoomCount = new Select(HomePage.PackageScreen.drop_noOfRoomsBox(driver));
	ArrayList<WebElement> RoomCountOptionList = new ArrayList<WebElement>(selectRoomCount.getOptions());
	ArrayList<String> StringRoomCountOptionList = new ArrayList<String>(RoomCountOptionList.size());
	
	for (Iterator iterator = RoomCountOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringRoomCountOptionList.add(element.getText());	
	}
		
	HolidayBookingEngine.setRoomCountList(StringRoomCountOptionList);
	HolidayBookingEngine.setRoomDefaultSelectedCount(selectRoomCount.getFirstSelectedOption().getText());
	
	
//>>>To get options in Adult count drop down>>>
	HolidayBookingEngine.setDropAdultCount(HomePage.PackageScreen.drop_noOfAdultsBox(driver).isDisplayed());
	
	Select selectAdultCount = new Select(HomePage.PackageScreen.drop_noOfAdultsBox(driver));
	ArrayList<WebElement> AdultCountOptionList = new ArrayList<WebElement>(selectAdultCount.getOptions());
	ArrayList<String> StringAdultCountOptionList = new ArrayList<String>(AdultCountOptionList.size());
		
	for (Iterator iterator = AdultCountOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringAdultCountOptionList.add(element.getText());	
	}
		
	HolidayBookingEngine.setAdultCountList(StringAdultCountOptionList);
	HolidayBookingEngine.setAdultDefaultSelectedCount(selectAdultCount.getFirstSelectedOption().getText());
	
	
//>>>To get options in Child count drop down>>>
	HolidayBookingEngine.setDropChildCount(HomePage.PackageScreen.drop_noOfChildrenBox(driver).isDisplayed());
		
	Select selectChildCount = new Select(HomePage.PackageScreen.drop_noOfChildrenBox(driver));
	ArrayList<WebElement> ChildCountOptionList = new ArrayList<WebElement>(selectChildCount.getOptions());
	ArrayList<String> StringChildCountOptionList = new ArrayList<String>(ChildCountOptionList.size());
		
	for (Iterator iterator = ChildCountOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringChildCountOptionList.add(element.getText());	
	}
	
	HolidayBookingEngine.setChildCountList(StringAdultCountOptionList);
	HolidayBookingEngine.setChildDefaultSelectedCount(selectChildCount.getFirstSelectedOption().getText());
	
	
//>>>To get options in Infant count drop down>>>
	HolidayBookingEngine.setDropInfantCount(HomePage.PackageScreen.drop_noOfInfantsBox(driver).isDisplayed());
			
	Select selectInfantCount = new Select(HomePage.PackageScreen.drop_noOfInfantsBox(driver));
	ArrayList<WebElement> InfantCountOptionList = new ArrayList<WebElement>(selectInfantCount.getOptions());
	ArrayList<String> StringInfantCountOptionList = new ArrayList<String>(InfantCountOptionList.size());
			
	for (Iterator iterator = InfantCountOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringInfantCountOptionList.add(element.getText());	
	}
			
	HolidayBookingEngine.setInfantCountList(StringInfantCountOptionList);
	HolidayBookingEngine.setInfantDefaultSelectedCount(selectInfantCount.getFirstSelectedOption().getText());


//>>>To get options in Seat class drop down>>>
	HolidayBookingEngine.setDropSeatClass(HomePage.PackageScreen.drop_seatClassBox(driver).isDisplayed());
			
	Select selectseatClass = new Select(HomePage.PackageScreen.drop_seatClassBox(driver));
	ArrayList<WebElement> SeatClassOptionList = new ArrayList<WebElement>(selectseatClass.getOptions());
	ArrayList<String> StringSeatClassOptionList = new ArrayList<String>(SeatClassOptionList.size());
			
	for (Iterator iterator = SeatClassOptionList.iterator(); iterator.hasNext();) {
		WebElement element = (WebElement) iterator.next();
		StringSeatClassOptionList.add(element.getText());	
	}
			
	HolidayBookingEngine.setSeatClassOptionsList(StringInfantCountOptionList);
	HolidayBookingEngine.setDefaultSeatClass(selectInfantCount.getFirstSelectedOption().getText());
	
	
//Getting the default selected product type
	boolean DefaultSelectedPackage    = HomePage.PackageScreen.radio_flightHotelButton(driver).isSelected();
	
	boolean ButtonFlightHotelCar = HomePage.PackageScreen.radio_flightHotelCarButton(driver).isDisplayed();
	boolean ButtonFlightHotelActivities = HomePage.PackageScreen.radio_flightHotelActivitiesButton(driver).isDisplayed();
	boolean LabelFlightHotel = HomePage.PackageScreen.label_flightHotel(driver).isDisplayed();
	boolean LabelFlightHotelCar = HomePage.PackageScreen.label_flightHotelCar(driver).isDisplayed();
	boolean LabelFlightHotelActivities = HomePage.PackageScreen.label_flightHotelActivities(driver).isDisplayed();
	boolean LabelFrom = HomePage.PackageScreen.label_flightHotel(driver).isDisplayed();
	boolean TextFrom = HomePage.PackageScreen.text_fromField(driver).isDisplayed();
	boolean LabelTo = HomePage.PackageScreen.label_to(driver).isDisplayed();
	boolean TextTo = HomePage.PackageScreen.text_toField(driver).isDisplayed();
	boolean LinkAllCities = HomePage.PackageScreen.button_allCitiesButton(driver).isDisplayed();
	boolean LinkDestinationCities = HomePage.PackageScreen.button_allDestinationsButton(driver).isDisplayed();
	boolean LabelDepartDate = HomePage.PackageScreen.label_departDate(driver).isDisplayed();
	boolean DTPDepartDate = HomePage.PackageScreen.dtp_departDate(driver).isDisplayed();
	boolean LabelDepartTime = HomePage.PackageScreen.label_departTime(driver).isDisplayed();	
	boolean DropDepartTime = HomePage.PackageScreen.drop_departTimeBox(driver).isDisplayed();
	boolean LabelReturnDate = HomePage.PackageScreen.label_returnDate(driver).isDisplayed();
	boolean DTPReturnDate = HomePage.PackageScreen.dtp_returnDate(driver).isDisplayed();
	boolean LabelReturnTime = HomePage.PackageScreen.label_returnTime(driver).isDisplayed();
	boolean DropReturnTime = HomePage.PackageScreen.drop_returnTimeBox(driver).isDisplayed();
	boolean LabelRoomCount = HomePage.PackageScreen.label_noOfRooms(driver).isDisplayed();
	boolean DropRoomCount = HomePage.PackageScreen.drop_noOfRoomsBox(driver).isDisplayed();
	boolean LabelAdultCount = HomePage.PackageScreen.label_noOfAdults(driver).isDisplayed();
	boolean LabelAdultAge = HomePage.PackageScreen.label_adultAge(driver).isDisplayed();
	boolean DropAdultCount = HomePage.PackageScreen.drop_noOfAdultsBox(driver).isDisplayed();
	boolean LabelChildCount = HomePage.PackageScreen.label_noOfChildren(driver).isDisplayed();
	boolean LabelChildAge = HomePage.PackageScreen.label_childrenAge(driver).isDisplayed();
	boolean DropChildCount = HomePage.PackageScreen.drop_noOfChildrenBox(driver).isDisplayed();
	boolean LabelInfantCount = HomePage.PackageScreen.label_noOfInfants(driver).isDisplayed();
	boolean LabelInfantAge = HomePage.PackageScreen.label_infantAge(driver).isDisplayed();
	boolean DropInfantCount = HomePage.PackageScreen.drop_noOfInfantsBox(driver).isDisplayed();
	boolean LabelSeatClass = HomePage.PackageScreen.label_seatClass(driver).isDisplayed();
	boolean DropSeatClass = HomePage.PackageScreen.drop_seatClassBox(driver).isDisplayed();
	boolean LabelPromotionCode = HomePage.PackageScreen.label_promotionCode(driver).isDisplayed();
	boolean TextPromotionCode = HomePage.PackageScreen.text_PromotionCode(driver).isDisplayed();
	boolean CheckDirectFirst = HomePage.PackageScreen.check_directFirstBox(driver).isDisplayed();
	boolean LabelDirectFirst = HomePage.PackageScreen.label_directFirst(driver).isDisplayed();
	boolean CheckPartialStay = HomePage.PackageScreen.check_partialStayBox(driver).isDisplayed();
	boolean LabelPartialStay = HomePage.PackageScreen.label_partialStay(driver).isDisplayed();
	boolean DTPPartialStayCheckInDate = HomePage.PackageScreen.dtp_partialSatyCheckInDate(driver).isDisplayed();
	boolean LabelPartialStayCheckInDate = HomePage.PackageScreen.label_partialStayCheckInDate(driver).isDisplayed();
	boolean DTPPartialStayCheckOutDate = HomePage.PackageScreen.dtp_partialSatyCheckOutDate(driver).isDisplayed();
	boolean LabelPartialStayCheckOutDate = HomePage.PackageScreen.label_partialStayCheckOutDate(driver).isDisplayed();
	boolean CheckHotelOtherCity = HomePage.PackageScreen.check_hotelOtherCityBox(driver).isDisplayed();
	boolean LabelHotelOtherCity = HomePage.PackageScreen.label_hotelOtherCity(driver).isDisplayed();
	boolean TextPartialStayCity = HomePage.PackageScreen.text_partialStayCityField(driver).isDisplayed();
	boolean PartialStayCity = HomePage.PackageScreen.label_partialStayCity(driver).isDisplayed();
	boolean ButtonSearch = HomePage.PackageScreen.button_searchButton(driver).isDisplayed();
	 
	 /*
	  * default selected value in language drop down
	  *All columbian cities
	  *All destination cities
	  *default date of destination
	  *return date
	  *default value in depart time
	  */
	 
}

//-------------------------------------------------------------------------------Hotel Search---------------------------------------------------------------------------
public boolean doHotelSearch(WebDriver driver, HotelSearchScenario search){
	try {
		
	//Selecting Location
		HomePage.HotelScreen.text_hotelNameField(driver).sendKeys(search.getToLocation());
		
	//All Destinations
		HomePage.HotelScreen.button_allDestinationsButton(driver).click();
		
	//Selecting Check in date
		HomePage.HotelScreen.dtp_checkInDate(driver).click();
		
	//Selecting Checkout date
		HomePage.HotelScreen.dtp_checkOutDate(driver).click();
		
	//Selecting Room count
		
	//Selecting Room occupancy
		
	//Selecting Hotel Name
		HomePage.HotelScreen.text_hotelNameField(driver).sendKeys(search.getHotelName());
		
	//Selecting Star Rating
		
	//Selecting Search Button
		HomePage.HotelScreen.button_searchButton(driver).click();
		
		return true;
	} catch (Exception e) {
		return false;
	}
}

//Selecting Hotel Room count
public boolean selectHotelRoomCount(WebDriver driver,String RoomCount){
	try {
		Select select = new Select(HomePage.HotelScreen.drop_noOfRoomsBox(driver));
		select.selectByVisibleText(RoomCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Adult Count
public boolean selectHotelAdultCount(WebDriver driver,String AdultCount){
	try {
		Select select = new Select(HomePage.HotelScreen.drop_noOfAdultsBox(driver));
		select.selectByVisibleText(AdultCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Child Count
public boolean selectHotelChildCount(WebDriver driver,String ChildCount){
	try {
		Select select = new Select(HomePage.HotelScreen.drop_noOfChildrenBox(driver));
		select.selectByVisibleText(ChildCount);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Star Rating
public boolean selectHotelStarRating(WebDriver driver,String StarRating){
	try {
		Select select = new Select(HomePage.HotelScreen.drop_starRatingBox(driver));
		select.selectByVisibleText(StarRating);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>To Check Hotel Elements>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public void checkHotelBookingEngine(WebDriver driver){
	
	ExpectedHotelBookingEngine  HotelBookingEngine = new ExpectedHotelBookingEngine();
	
//>>>To get options in Room count drop down>>>
	HotelBookingEngine.setDropRoomCount(HomePage.HotelScreen.drop_noOfRoomsBox(driver).isDisplayed());
	
	Select selectRoomCount = new Select(HomePage.HotelScreen.drop_noOfRoomsBox(driver));
	ArrayList<WebElement> RoomCountOptionList = new ArrayList<WebElement>(selectRoomCount.getOptions());
	ArrayList<String> StringRoomCountOptionList = new ArrayList<String>(RoomCountOptionList.size());
		
	for (Iterator iterator = RoomCountOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringRoomCountOptionList.add(element.getText());
	}
		
	HotelBookingEngine.setRoomCountOptionsList(StringRoomCountOptionList);
	HotelBookingEngine.setDefaultRoomCount(selectRoomCount.getFirstSelectedOption().getText());
	
	
//>>>To get options in Adult count drop down>>>
	HotelBookingEngine.setDropAdultCount(HomePage.HotelScreen.drop_noOfAdultsBox(driver).isDisplayed());
		
	Select selectAdultCount = new Select(HomePage.HotelScreen.drop_noOfAdultsBox(driver));
	ArrayList<WebElement> AdultCountOptionList = new ArrayList<WebElement>(selectAdultCount.getOptions());
	ArrayList<String> StringAdultCountOptionList = new ArrayList<String>(AdultCountOptionList.size());
			
	for (Iterator iterator = AdultCountOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringAdultCountOptionList.add(element.getText());
	}
		
	HotelBookingEngine.setAdultCountOptionsList(StringAdultCountOptionList);
	HotelBookingEngine.setDefaultAdultCount(selectAdultCount.getFirstSelectedOption().getText());
	
	
//>>>To get options in Child count drop down>>>
	HotelBookingEngine.setDropChildCount(HomePage.HotelScreen.drop_noOfChildrenBox(driver).isDisplayed());
			
	Select selectChildCount = new Select(HomePage.HotelScreen.drop_noOfChildrenBox(driver));
	ArrayList<WebElement> ChildCountOptionList = new ArrayList<WebElement>(selectChildCount.getOptions());
	ArrayList<String> StringChildCountOptionList = new ArrayList<String>(ChildCountOptionList.size());
				
	for (Iterator iterator = ChildCountOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringChildCountOptionList.add(element.getText());
	}
		
	HotelBookingEngine.setChildCountOptionsList(StringChildCountOptionList);
	HotelBookingEngine.setDefaultChildCount(selectChildCount.getFirstSelectedOption().getText());
	
	
//>>>To get options in Child count drop down>>>
	HotelBookingEngine.setDropStarRating(HomePage.HotelScreen.drop_starRatingBox(driver).isDisplayed());
				
	Select selectStarRating = new Select(HomePage.HotelScreen.drop_starRatingBox(driver));
	ArrayList<WebElement> StarRatingOptionList = new ArrayList<WebElement>(selectStarRating.getOptions());
	ArrayList<String> StringStarRatingOptionList = new ArrayList<String>(StarRatingOptionList.size());
					
	for (Iterator iterator = StarRatingOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringStarRatingOptionList.add(element.getText());
	}
			
	HotelBookingEngine.setStarRatingOptionsList(StringStarRatingOptionList);
	HotelBookingEngine.setDefaultStarRating(selectStarRating.getFirstSelectedOption().getText());
	
	
	
	boolean LabelLocation = HomePage.HotelScreen.label_location(driver).isDisplayed();
	boolean TextLocation = HomePage.HotelScreen.text_locationField(driver).isDisplayed();
	boolean LinkLocation = HomePage.HotelScreen.button_allDestinationsButton(driver).isDisplayed();
	boolean LabelCheckInDate = HomePage.HotelScreen.label_checkInDate(driver).isDisplayed();
	boolean DTPCheckInDate = HomePage.HotelScreen.dtp_checkInDate(driver).isDisplayed();
	boolean LabelCheckOutDate = HomePage.HotelScreen.label_checkOutDate(driver).isDisplayed();
	boolean DTPCheckOutDate = HomePage.HotelScreen.dtp_checkOutDate(driver).isDisplayed();
	boolean LabelRooms = HomePage.HotelScreen.label_noOfRooms(driver).isDisplayed();
	boolean DropRooms = HomePage.HotelScreen.drop_noOfRoomsBox(driver).isDisplayed();
	boolean LabelAdults = HomePage.HotelScreen.label_noOfAdults(driver).isDisplayed();
	boolean LabelAdultAge = HomePage.HotelScreen.label_adultAge(driver).isDisplayed();
	boolean DropAdults = HomePage.HotelScreen.drop_noOfAdultsBox(driver).isDisplayed();
	boolean LabelChildren = HomePage.HotelScreen.label_noOfChildren(driver).isDisplayed();
	boolean LabelChildAge = HomePage.HotelScreen.label_childAge(driver).isDisplayed();
	boolean DropChildren = HomePage.HotelScreen.drop_noOfChildrenBox(driver).isDisplayed();
	boolean LabelHotelName = HomePage.HotelScreen.label_hotelName(driver).isDisplayed();
	boolean TextHotelName = HomePage.HotelScreen.text_hotelNameField(driver).isDisplayed();
	boolean LabelStarRating = HomePage.HotelScreen.label_starRating(driver).isDisplayed();
	boolean DropStarRating = HomePage.HotelScreen.drop_starRatingBox(driver).isDisplayed();
	boolean ButtonSearch = HomePage.HotelScreen.button_searchButton(driver).isDisplayed();
}

//-------------------------------------------------------------------------------Car Search--------------------------------------------------------------------

public boolean doCarSearch(WebDriver driver, CarSearchScenario search){
	try {
		
	//Selecting Pickup Location
		HomePage.CarScreen.text_pickupLocationField(driver).sendKeys(search.getPickupLocation());
		
	//Selecting Return Location
		HomePage.CarScreen.text_returnLocationField(driver).sendKeys(search.getReturnLocation());
		
	//Selecting Return to Pickup Location
		HomePage.CarScreen.check_returnToPickupLocationBox(driver).click();
		
	//Selecting Pickup Date
		HomePage.CarScreen.dtp_pickupDate(driver).click();
		
	//Selecting Return Date
		HomePage.CarScreen.dtp_returnDate(driver).click();
		
		return true;
	} catch (Exception e) {
		return false;
	}
}

//Selecting Car Pickup Time
public boolean selectCarPickupTime(WebDriver driver,String PickupTime){
	try {
		Select select = new Select(HomePage.CarScreen.drop_pickupTimeBox(driver));
		select.selectByVisibleText(PickupTime);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Car Return Time
public boolean selectCarReturnTime(WebDriver driver,String ReturnTime){
	try {
		Select select = new Select(HomePage.CarScreen.drop_returnTimeBox(driver));
		select.selectByVisibleText(ReturnTime);
		return true;
		} catch (Exception e) {
			return false;
		}
}

//Selecting Car Type
public boolean selectCarType(WebDriver driver,String CarType){
	try {
		Select select = new Select(HomePage.CarScreen.drop_carTypeBox(driver));
		select.selectByVisibleText(CarType);
		return true;
		} catch (Exception e) {
			return false;
		}
}


//>>>>>>>>>>>>>>>>>>>>>>To check Car elements>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public void checkCarBookingEngine(WebDriver driver){
	
	ExpectedCarBookingEngine  CarBookingEngine = new ExpectedCarBookingEngine();
	
	
//>>>To get options in Pick up time drop down>>>
	CarBookingEngine.setDropPickUpTime(HomePage.CarScreen.drop_pickupTimeBox(driver).isDisplayed());
	
	Select selectPickUpTime = new Select(HomePage.CarScreen.drop_pickupTimeBox(driver));
	ArrayList<WebElement> PickUpTimeOptionList = new ArrayList<WebElement>(selectPickUpTime.getOptions());
	ArrayList<String> StringPickUpTimeOptionList = new ArrayList<String>(PickUpTimeOptionList.size());
			
	for (Iterator iterator = PickUpTimeOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringPickUpTimeOptionList.add(element.getText());
	}
			
	CarBookingEngine.setPickUpTimeOptionsList(StringPickUpTimeOptionList);
	CarBookingEngine.setDefaultPickUpTime(selectPickUpTime.getFirstSelectedOption().getText());
	
	
//>>>To get options in Return time drop down>>>
	CarBookingEngine.setDropReturnTime(HomePage.CarScreen.drop_returnTimeBox(driver).isDisplayed());
		
	Select selectReturnTime = new Select(HomePage.CarScreen.drop_returnTimeBox(driver));
	ArrayList<WebElement> ReturnTimeOptionList = new ArrayList<WebElement>(selectReturnTime.getOptions());
	ArrayList<String> StringReturnTimeOptionList = new ArrayList<String>(ReturnTimeOptionList.size());
				
	for (Iterator iterator = ReturnTimeOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringReturnTimeOptionList.add(element.getText());
	}
				
	CarBookingEngine.setReturnTimeOptionsList(StringReturnTimeOptionList);
	CarBookingEngine.setDefaultReturnTime(selectReturnTime.getFirstSelectedOption().getText());
	
	
//>>>To get options in Pick up time drop down>>>
	CarBookingEngine.setDropCarType(HomePage.CarScreen.drop_carTypeBox(driver).isDisplayed());
			
	Select selectCarType = new Select(HomePage.CarScreen.drop_carTypeBox(driver));
	ArrayList<WebElement> CarTypeOptionList = new ArrayList<WebElement>(selectCarType.getOptions());
	ArrayList<String> StringCarTypeOptionList = new ArrayList<String>(CarTypeOptionList.size());
					
	for (Iterator iterator = CarTypeOptionList.iterator(); iterator.hasNext();){
		WebElement element = (WebElement) iterator.next();
		StringCarTypeOptionList.add(element.getText());
	}
					
	CarBookingEngine.setCarTypeOptionsList(StringCarTypeOptionList);
	CarBookingEngine.setDefaultCarType(selectCarType.getFirstSelectedOption().getText());
	
	
	
	boolean LabelPickUpLocation = HomePage.CarScreen.label_pickupLocation(driver).isDisplayed();
	boolean TextPickUpLocation = HomePage.CarScreen.text_pickupLocationField(driver).isDisplayed();
	boolean LabelReturnLocation = HomePage.CarScreen.label_returnLocation(driver).isDisplayed();
	boolean TextReturnLocation = HomePage.CarScreen.text_returnLocationField(driver).isDisplayed();
	boolean LabelReturnToPickUpLocation = HomePage.CarScreen.label_returnToPickupLocation(driver).isDisplayed();
	boolean CheckReturnToPickupLocation = HomePage.CarScreen.check_returnToPickupLocationBox(driver).isDisplayed();
	boolean LabelPickUpDate = HomePage.CarScreen.label_pickupDate(driver).isDisplayed();
	boolean DTPPickUpDate = HomePage.CarScreen.dtp_pickupDate(driver).isDisplayed();
	boolean LabelPickUpTime = HomePage.CarScreen.label_pickupTime(driver).isDisplayed();
	boolean DropPickUpTime = HomePage.CarScreen.drop_pickupTimeBox(driver).isDisplayed();
	boolean LabelReturnDate = HomePage.CarScreen.label_returnDate(driver).isDisplayed();
	boolean DTPReturnDate = HomePage.CarScreen.dtp_returnDate(driver).isDisplayed();
	boolean LabelReturnTime = HomePage.CarScreen.label_returnTime(driver).isDisplayed();
	boolean DropReturnTime = HomePage.CarScreen.drop_returnTimeBox(driver).isDisplayed();
	boolean LabelCarType = HomePage.CarScreen.label_carType(driver).isDisplayed();
	boolean DropCarType = HomePage.CarScreen.drop_carTypeBox(driver).isDisplayed();
	boolean ButtonSearch = HomePage.CarScreen.button_searchButton(driver).isDisplayed();
}

}
