package auto.avianca.holidays.web.pojo;

public class ExpectedCarResult {
	
	private ExpectedCarBookingEngine 	CarBookingEngine 	= null;
	private ExpectedCarResultPage 		CarResultPage 		= null;
	private ExpectedCarCart 			CarCart 			= null;
	
	
	public ExpectedCarBookingEngine getCarBookingEngine() {
		return CarBookingEngine;
	}
	public void setCarBookingEngine(ExpectedCarBookingEngine carBookingEngine) {
		CarBookingEngine = carBookingEngine;
	}
	public ExpectedCarResultPage getCarResultPage() {
		return CarResultPage;
	}
	public void setCarResultPage(ExpectedCarResultPage carResultPage) {
		CarResultPage = carResultPage;
	}
	public ExpectedCarCart getCarCart() {
		return CarCart;
	}
	public void setCarCart(ExpectedCarCart carCart) {
		CarCart = carCart;
	}
	

}
