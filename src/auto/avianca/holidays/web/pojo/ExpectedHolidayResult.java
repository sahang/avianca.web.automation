package auto.avianca.holidays.web.pojo;

public class ExpectedHolidayResult {
	
private ExpectedHolidayBookingEngine HolidayBookingEngine = null;
private ExpectedHolidayResultPage    HolidayResultsPage   = null;
private ExpectedHolidayCart          HolidayResultsCart   = null;


public ExpectedHolidayBookingEngine getHolidayBookingEngine() {
	return HolidayBookingEngine;
}
public void setHolidayBookingEngine(ExpectedHolidayBookingEngine holidayBookingEngine) {
	HolidayBookingEngine = holidayBookingEngine;
}
public ExpectedHolidayResultPage getHolidayResultsPage() {
	return HolidayResultsPage;
}
public void setHolidayResultsPage(ExpectedHolidayResultPage holidayResultsPage) {
	HolidayResultsPage = holidayResultsPage;
}
public ExpectedHolidayCart getHolidayResultsCart() {
	return HolidayResultsCart;
}
public void setHolidayResultsCart(ExpectedHolidayCart holidayResultsCart) {
	HolidayResultsCart = holidayResultsCart;
}

}
