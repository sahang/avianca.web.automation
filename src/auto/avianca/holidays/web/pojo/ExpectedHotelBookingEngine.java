package auto.avianca.holidays.web.pojo;

import java.util.ArrayList;

public class ExpectedHotelBookingEngine {
	
	private boolean LabelLocation 						= false;
	private boolean TextLocation 						= false;
	private boolean LinkDestinations 					= false;
	private boolean LabelCheckInDate 					= false;
	private boolean DTPCheckInDate 						= false;
	private boolean LabelCheckOutDate 					= false;
	private boolean DTPCheckOutDate 					= false;
	private boolean LabelRoomCount 						= false;
	private boolean DropRoomCount 						= false;
	private boolean LabelAdultCount 					= false;
	private boolean LabelAdultAge 						= false;
	private boolean DropAdultCount 						= false;
	private boolean LabelChildCount 					= false;
	private boolean LabelChildAge 						= false;
	private boolean DropChildCount 						= false;
	private boolean LabelHotelName 						= false;
	private boolean TextHotelName 						= false;
	private boolean LabelStarRating 					= false;
	private boolean DropStarRating 						= false;
	private boolean ButtonSearch 						= false;
	
	
	private String LocationLabel 						= "N/A";
	private String CheckInDateLabel 					= "N/A";
	private String CheckOutLabel 						= "N/A";
	private String RoomCountLabel 						= "N/A";
	private String AdultCountLabel 						= "N/A";
	private String AdultAgeLabel 						= "N/A";
	private String ChildCountLabel 						= "N/A";
	private String ChildAgeLabel 						= "N/A";
	private String HotelNameLabel 						= "N/A";
	private String StarRatingLabel 						= "N/A";
	
	
	private String DefaultRoomCount						= "N/A";
	private ArrayList<String> RoomCountOptionsList 		= null;
	private String DefaultAdultCount 					= "N/A";
	private ArrayList<String> AdultCountOptionsList 	= null;
	private String DefaultChildCount 					= "N/A";
	private ArrayList<String> ChildCountOptionsList 	= null;
	private String DefaultStarRating 					= "N/A";
	private ArrayList<String> StarRatingOptionsList 	= null;
	
	
	
	public boolean isLabelLocation() {
		return LabelLocation;
	}
	public void setLabelLocation(boolean labelLocation) {
		LabelLocation = labelLocation;
	}
	public boolean isTextLocation() {
		return TextLocation;
	}
	public void setTextLocation(boolean textLocation) {
		TextLocation = textLocation;
	}
	public boolean isLinkDestinations() {
		return LinkDestinations;
	}
	public void setLinkDestinations(boolean linkDestinations) {
		LinkDestinations = linkDestinations;
	}
	public boolean isLabelCheckInDate() {
		return LabelCheckInDate;
	}
	public void setLabelCheckInDate(boolean labelCheckInDate) {
		LabelCheckInDate = labelCheckInDate;
	}
	public boolean isDTPCheckInDate() {
		return DTPCheckInDate;
	}
	public void setDTPCheckInDate(boolean dTPCheckInDate) {
		DTPCheckInDate = dTPCheckInDate;
	}
	public boolean isLabelCheckOutDate() {
		return LabelCheckOutDate;
	}
	public void setLabelCheckOutDate(boolean labelCheckOutDate) {
		LabelCheckOutDate = labelCheckOutDate;
	}
	public boolean isDTPCheckOutDate() {
		return DTPCheckOutDate;
	}
	public void setDTPCheckOutDate(boolean dTPCheckOutDate) {
		DTPCheckOutDate = dTPCheckOutDate;
	}
	public boolean isLabelRoomCount() {
		return LabelRoomCount;
	}
	public void setLabelRoomCount(boolean labelRoomCount) {
		LabelRoomCount = labelRoomCount;
	}
	public boolean isDropRoomCount() {
		return DropRoomCount;
	}
	public void setDropRoomCount(boolean dropRoomCount) {
		DropRoomCount = dropRoomCount;
	}
	public boolean isLabelAdultCount() {
		return LabelAdultCount;
	}
	public void setLabelAdultCount(boolean labelAdultCount) {
		LabelAdultCount = labelAdultCount;
	}
	public boolean isLabelAdultAge() {
		return LabelAdultAge;
	}
	public void setLabelAdultAge(boolean labelAdultAge) {
		LabelAdultAge = labelAdultAge;
	}
	public boolean isDropAdultCount() {
		return DropAdultCount;
	}
	public void setDropAdultCount(boolean dropAdultCount) {
		DropAdultCount = dropAdultCount;
	}
	public boolean isLabelChildCount() {
		return LabelChildCount;
	}
	public void setLabelChildCount(boolean labelChildCount) {
		LabelChildCount = labelChildCount;
	}
	public boolean isLabelChildAge() {
		return LabelChildAge;
	}
	public void setLabelChildAge(boolean labelChildAge) {
		LabelChildAge = labelChildAge;
	}
	public boolean isDropChildCount() {
		return DropChildCount;
	}
	public void setDropChildCount(boolean dropChildCount) {
		DropChildCount = dropChildCount;
	}
	public boolean isLabelHotelName() {
		return LabelHotelName;
	}
	public void setLabelHotelName(boolean labelHotelName) {
		LabelHotelName = labelHotelName;
	}
	public boolean isTextHotelName() {
		return TextHotelName;
	}
	public void setTextHotelName(boolean textHotelName) {
		TextHotelName = textHotelName;
	}
	public boolean isLabelStarRating() {
		return LabelStarRating;
	}
	public void setLabelStarRating(boolean labelStarRating) {
		LabelStarRating = labelStarRating;
	}
	public boolean isDropStarRating() {
		return DropStarRating;
	}
	public void setDropStarRating(boolean dropStarRating) {
		DropStarRating = dropStarRating;
	}
	public boolean isButtonSearch() {
		return ButtonSearch;
	}
	public void setButtonSearch(boolean buttonSearch) {
		ButtonSearch = buttonSearch;
	}
	public String getLocationLabel() {
		return LocationLabel;
	}
	public void setLocationLabel(String locationLabel) {
		LocationLabel = locationLabel;
	}
	public String getCheckInDateLabel() {
		return CheckInDateLabel;
	}
	public void setCheckInDateLabel(String checkInDateLabel) {
		CheckInDateLabel = checkInDateLabel;
	}
	public String getCheckOutLabel() {
		return CheckOutLabel;
	}
	public void setCheckOutLabel(String checkOutLabel) {
		CheckOutLabel = checkOutLabel;
	}
	public String getRoomCountLabel() {
		return RoomCountLabel;
	}
	public void setRoomCountLabel(String roomCountLabel) {
		RoomCountLabel = roomCountLabel;
	}
	public String getAdultCountLabel() {
		return AdultCountLabel;
	}
	public void setAdultCountLabel(String adultCountLabel) {
		AdultCountLabel = adultCountLabel;
	}
	public String getAdultAgeLabel() {
		return AdultAgeLabel;
	}
	public void setAdultAgeLabel(String adultAgeLabel) {
		AdultAgeLabel = adultAgeLabel;
	}
	public String getChildCountLabel() {
		return ChildCountLabel;
	}
	public void setChildCountLabel(String childCountLabel) {
		ChildCountLabel = childCountLabel;
	}
	public String getChildAgeLabel() {
		return ChildAgeLabel;
	}
	public void setChildAgeLabel(String childAgeLabel) {
		ChildAgeLabel = childAgeLabel;
	}
	public String getHotelNameLabel() {
		return HotelNameLabel;
	}
	public void setHotelNameLabel(String hotelNameLabel) {
		HotelNameLabel = hotelNameLabel;
	}
	public String getStarRatingLabel() {
		return StarRatingLabel;
	}
	public void setStarRatingLabel(String starRatingLabel) {
		StarRatingLabel = starRatingLabel;
	}
	public String getDefaultRoomCount() {
		return DefaultRoomCount;
	}
	public void setDefaultRoomCount(String defaultRoomCount) {
		DefaultRoomCount = defaultRoomCount;
	}
	public ArrayList<String> getRoomCountOptionsList() {
		return RoomCountOptionsList;
	}
	public void setRoomCountOptionsList(ArrayList<String> roomCountOptionsList) {
		RoomCountOptionsList = roomCountOptionsList;
	}
	public String getDefaultAdultCount() {
		return DefaultAdultCount;
	}
	public void setDefaultAdultCount(String defaultAdultCount) {
		DefaultAdultCount = defaultAdultCount;
	}
	public ArrayList<String> getAdultCountOptionsList() {
		return AdultCountOptionsList;
	}
	public void setAdultCountOptionsList(ArrayList<String> adultCountOptionsList) {
		AdultCountOptionsList = adultCountOptionsList;
	}
	public String getDefaultChildCount() {
		return DefaultChildCount;
	}
	public void setDefaultChildCount(String defaultChildCount) {
		DefaultChildCount = defaultChildCount;
	}
	public ArrayList<String> getChildCountOptionsList() {
		return ChildCountOptionsList;
	}
	public void setChildCountOptionsList(ArrayList<String> childCountOptionsList) {
		ChildCountOptionsList = childCountOptionsList;
	}
	public String getDefaultStarRating() {
		return DefaultStarRating;
	}
	public void setDefaultStarRating(String defaultStarRating) {
		DefaultStarRating = defaultStarRating;
	}
	public ArrayList<String> getStarRatingOptionsList() {
		return StarRatingOptionsList;
	}
	public void setStarRatingOptionsList(ArrayList<String> starRatingOptionsList) {
		StarRatingOptionsList = starRatingOptionsList;
	}
	
}
