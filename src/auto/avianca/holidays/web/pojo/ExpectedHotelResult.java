package auto.avianca.holidays.web.pojo;

public class ExpectedHotelResult {
	
	private ExpectedHotelBookingEngine 	HotelBookingEngine 	= null;
	private ExpectedHotelResultPage 	HotelResultsPage 	= null;
	private ExpectedHotelCart 			HotelCart 			= null;
	
	public ExpectedHotelBookingEngine getHotelBookingEngine() {
		return HotelBookingEngine;
	}
	public void setHotelBookingEngine(ExpectedHotelBookingEngine hotelBookingEngine) {
		HotelBookingEngine = hotelBookingEngine;
	}
	public ExpectedHotelResultPage getHotelResultsPage() {
		return HotelResultsPage;
	}
	public void setHotelResultsPage(ExpectedHotelResultPage hotelResultsPage) {
		HotelResultsPage = hotelResultsPage;
	}
	public ExpectedHotelCart getHotelCart() {
		return HotelCart;
	}
	public void setHotelCart(ExpectedHotelCart hotelCart) {
		this.HotelCart = hotelCart;
	}
	
}
