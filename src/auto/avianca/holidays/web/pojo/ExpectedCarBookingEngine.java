package auto.avianca.holidays.web.pojo;

import java.util.ArrayList;

public class ExpectedCarBookingEngine {
	
	private boolean LabelPickUpLocation 				= false;
	private boolean TextPickUpLocation 					= false;
	private boolean LabelReturnLocation 				= false;
	private boolean TextReturnLocation 					= false;
	private boolean LabelReternToPickUpLocation 		= false;
	private boolean CheckReturnToPickUpLocation 		= false;
	private boolean LabelPickUpDate 					= false;
	private boolean DTPPickUpDate 						= false;
	private boolean LabelPickUpTime 					= false;
	private boolean DropPickUpTime 						= false;
	private boolean LabelReturnDate 					= false;
	private boolean DTPReturnDate 						= false;
	private boolean LabelReturnTime 					= false;
	private boolean DropReturnTime 						= false;
	private boolean LabelCarType 						= false;
	private boolean DropCarType 						= false;
	private boolean ButtonSearch 						= false;
	
	
	private String PickUpLocationLabel 					= "N/A";
	private String ReturnLocationLabel 					= "N/A";
	private String ReturnToPickUpLocationLabel 			= "N/A";
	private String PickUpDateLabel 						= "N/A";
	private String PickUpTimeLabel 						= "N/A";
	private String ReturnDateLabel 						= "N/A";
	private String ReturnTimeLabel 						= "N/A";
	private String CarTypeLabel 						= "N/A";
	
	
	private String DefaultPickUpTime					= "N/A";
	private ArrayList<String> PickUpTimeOptionsList 	= null;
	private String DefaultReturnTime					= "N/A";
	private ArrayList<String> ReturnTimeOptionsList 	= null;
	private String DefaultCarType						= "N/A";
	private ArrayList<String> CarTypeOptionsList 		= null;
	
	
	
	public boolean isLabelPickUpLocation() {
		return LabelPickUpLocation;
	}
	public void setLabelPickUpLocation(boolean labelPickUpLocation) {
		LabelPickUpLocation = labelPickUpLocation;
	}
	public boolean isTextPickUpLocation() {
		return TextPickUpLocation;
	}
	public void setTextPickUpLocation(boolean textPickUpLocation) {
		TextPickUpLocation = textPickUpLocation;
	}
	public boolean isLabelReturnLocation() {
		return LabelReturnLocation;
	}
	public void setLabelReturnLocation(boolean labelReturnLocation) {
		LabelReturnLocation = labelReturnLocation;
	}
	public boolean isTextReturnLocation() {
		return TextReturnLocation;
	}
	public void setTextReturnLocation(boolean textReturnLocation) {
		TextReturnLocation = textReturnLocation;
	}
	public boolean isLabelReternToPickUpLocation() {
		return LabelReternToPickUpLocation;
	}
	public void setLabelReternToPickUpLocation(boolean labelReternToPickUpLocation) {
		LabelReternToPickUpLocation = labelReternToPickUpLocation;
	}
	public boolean isCheckReturnToPickUpLocation() {
		return CheckReturnToPickUpLocation;
	}
	public void setCheckReturnToPickUpLocation(boolean checkReturnToPickUpLocation) {
		CheckReturnToPickUpLocation = checkReturnToPickUpLocation;
	}
	public boolean isLabelPickUpDate() {
		return LabelPickUpDate;
	}
	public void setLabelPickUpDate(boolean labelPickUpDate) {
		LabelPickUpDate = labelPickUpDate;
	}
	public boolean isDTPPickUpDate() {
		return DTPPickUpDate;
	}
	public void setDTPPickUpDate(boolean dTPPickUpDate) {
		DTPPickUpDate = dTPPickUpDate;
	}
	public boolean isLabelPickUpTime() {
		return LabelPickUpTime;
	}
	public void setLabelPickUpTime(boolean labelPickUpTime) {
		LabelPickUpTime = labelPickUpTime;
	}
	public boolean isDropPickUpTime() {
		return DropPickUpTime;
	}
	public void setDropPickUpTime(boolean dropPickUpTime) {
		DropPickUpTime = dropPickUpTime;
	}
	public boolean isLabelReturnDate() {
		return LabelReturnDate;
	}
	public void setLabelReturnDate(boolean labelReturnDate) {
		LabelReturnDate = labelReturnDate;
	}
	public boolean isDTPReturnDate() {
		return DTPReturnDate;
	}
	public void setDTPReturnDate(boolean dTPReturnDate) {
		DTPReturnDate = dTPReturnDate;
	}
	public boolean isLabelReturnTime() {
		return LabelReturnTime;
	}
	public void setLabelReturnTime(boolean labelReturnTime) {
		LabelReturnTime = labelReturnTime;
	}
	public boolean isDropReturnTime() {
		return DropReturnTime;
	}
	public void setDropReturnTime(boolean dropReturnTime) {
		DropReturnTime = dropReturnTime;
	}
	public boolean isLabelCarType() {
		return LabelCarType;
	}
	public void setLabelCarType(boolean labelCarType) {
		LabelCarType = labelCarType;
	}
	public boolean isDropCarType() {
		return DropCarType;
	}
	public void setDropCarType(boolean dropCarType) {
		DropCarType = dropCarType;
	}
	public boolean isButtonSearch() {
		return ButtonSearch;
	}
	public void setButtonSearch(boolean buttonSearch) {
		ButtonSearch = buttonSearch;
	}
	public String getPickUpLocationLabel() {
		return PickUpLocationLabel;
	}
	public void setPickUpLocationLabel(String pickUpLocationLabel) {
		PickUpLocationLabel = pickUpLocationLabel;
	}
	public String getReturnLocationLabel() {
		return ReturnLocationLabel;
	}
	public void setReturnLocationLabel(String returnLocationLabel) {
		ReturnLocationLabel = returnLocationLabel;
	}
	public String getReturnToPickUpLocationLabel() {
		return ReturnToPickUpLocationLabel;
	}
	public void setReturnToPickUpLocationLabel(String returnToPickUpLocationLabel) {
		ReturnToPickUpLocationLabel = returnToPickUpLocationLabel;
	}
	public String getPickUpDateLabel() {
		return PickUpDateLabel;
	}
	public void setPickUpDateLabel(String pickUpDateLabel) {
		PickUpDateLabel = pickUpDateLabel;
	}
	public String getPickUpTimeLabel() {
		return PickUpTimeLabel;
	}
	public void setPickUpTimeLabel(String pickUpTimeLabel) {
		PickUpTimeLabel = pickUpTimeLabel;
	}
	public String getReturnDateLabel() {
		return ReturnDateLabel;
	}
	public void setReturnDateLabel(String returnDateLabel) {
		ReturnDateLabel = returnDateLabel;
	}
	public String getReturnTimeLabel() {
		return ReturnTimeLabel;
	}
	public void setReturnTimeLabel(String returnTimeLabel) {
		ReturnTimeLabel = returnTimeLabel;
	}
	public String getCarTypeLabel() {
		return CarTypeLabel;
	}
	public void setCarTypeLabel(String carTypeLabel) {
		CarTypeLabel = carTypeLabel;
	}
	public String getDefaultPickUpTime() {
		return DefaultPickUpTime;
	}
	public void setDefaultPickUpTime(String defaultPickUpTime) {
		DefaultPickUpTime = defaultPickUpTime;
	}
	public ArrayList<String> getPickUpTimeOptionsList() {
		return PickUpTimeOptionsList;
	}
	public void setPickUpTimeOptionsList(ArrayList<String> pickUpTimeOptionsList) {
		PickUpTimeOptionsList = pickUpTimeOptionsList;
	}
	public String getDefaultReturnTime() {
		return DefaultReturnTime;
	}
	public void setDefaultReturnTime(String defaultReturnTime) {
		DefaultReturnTime = defaultReturnTime;
	}
	public ArrayList<String> getReturnTimeOptionsList() {
		return ReturnTimeOptionsList;
	}
	public void setReturnTimeOptionsList(ArrayList<String> returnTimeOptionsList) {
		ReturnTimeOptionsList = returnTimeOptionsList;
	}
	public String getDefaultCarType() {
		return DefaultCarType;
	}
	public void setDefaultCarType(String defaultCarType) {
		DefaultCarType = defaultCarType;
	}
	public ArrayList<String> getCarTypeOptionsList() {
		return CarTypeOptionsList;
	}
	public void setCarTypeOptionsList(ArrayList<String> carTypeOptionsList) {
		CarTypeOptionsList = carTypeOptionsList;
	}

}
