package auto.avianca.holidays.web.pojo;

import auto.avianca.common.web.enumtypes.DepartureType;
import auto.avianca.common.web.enumtypes.PackageType;
import auto.avianca.common.web.enumtypes.ReturnType;
import auto.avianca.common.web.enumtypes.SeatClass;
import auto.avianca.common.web.enumtypes.ValidationType;

public class PackageSearchScenario {

	private String DepartureDestination 		= "";
	private String ArrivalDestination 			= "";
	private String DepartureDate 				= "";
	private String ArrivalDate 					= "";
	private String DepartureTime 				= "";
	private String ArrivalTime 					= "";
	private String PromoCode 					= "";
	private String DirectFirst 					= "";
	private String PartialStayInHotel 			= "";
	private String PartialStayCheckInDate 		= "";
	private String PartialStayCheckOutDate 		= "";
	private String HotelInOtherCity 			= "";
	private String HotelInOtherCityName			= "";
	private String Language            			= "";
	private String AllCities					= "";
	private String AllDestinations				= "";
	private String RoomCount					= "";
	private String AdultCount					= "";
	private String ChildCount					= "";
	private String ChildAge						= "";
	private String InfantCount					= "";
	private ValidationType   TestType           = ValidationType.NONE;
	private PackageType HolidayPackageType 		= PackageType.NONE;
	private DepartureType DepartureTimeType     = DepartureType.NONE;
	private ReturnType ReturnTimeType			= ReturnType.NONE;
	private SeatClass SeatClassType 			= SeatClass.NONE;
	
	
	
	public ValidationType getTestType() {
		return TestType;
	}
	public void setTestType(ValidationType testType) {
		TestType = testType;
	}
	public String getHotelInOtherCityName() {
		return HotelInOtherCityName;
	}
	public void setHotelInOtherCityName(String hotelInOtherCityName) {
		HotelInOtherCityName = hotelInOtherCityName;
	}
	public String getDirectFirst() {
		return DirectFirst;
	}
	public void setDirectFirst(String directFirst) {
		DirectFirst = directFirst;
	}
	public String getPartialStayInHotel() {
		return PartialStayInHotel;
	}
	public void setPartialStayInHotel(String partialStayInHotel) {
		PartialStayInHotel = partialStayInHotel;
	}
	public SeatClass getSeatClassType() {
		return SeatClassType;
	}
	public void setSeatClassType(SeatClass seatClassType) {
		SeatClassType = seatClassType;
	}
	public String getAdultCount() {
		return AdultCount;
	}
	public void setAdultCount(String adultCount) {
		AdultCount = adultCount;
	}
	public String getChildCount() {
		return ChildCount;
	}
	public void setChildCount(String childCount) {
		ChildCount = childCount;
	}
	public String getChildAge() {
		return ChildAge;
	}
	public void setChildAge(String childAge) {
		ChildAge = childAge;
	}
	public String getInfantCount() {
		return InfantCount;
	}
	public void setInfantCount(String infantCount) {
		InfantCount = infantCount;
	}
	public String getPartialStayCheckInDate() {
		return PartialStayCheckInDate;
	}
	public void setPartialStayCheckInDate(String partialStayCheckInDate) {
		PartialStayCheckInDate = partialStayCheckInDate;
	}
	public String getPartialStayCheckOutDate() {
		return PartialStayCheckOutDate;
	}
	public void setPartialStayCheckOutDate(String partialStayCheckOutDate) {
		PartialStayCheckOutDate = partialStayCheckOutDate;
	}
	public String getRoomCount() {
		return RoomCount;
	}
	public void setRoomCount(String roomCount) {
		RoomCount = roomCount;
	}
	public ReturnType getReturnTimeType() {
		return ReturnTimeType;
	}
	public void setReturnTimeType(ReturnType returnTimeType) {
		ReturnTimeType = returnTimeType;
	}
	public String getAllCities() {
		return AllCities;
	}
	public void setAllCities(String allCities) {
		AllCities = allCities;
	}
	public String getAllDestinations() {
		return AllDestinations;
	}
	public void setAllDestinations(String allDestinations) {
		AllDestinations = allDestinations;
	}
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	public DepartureType getDepartureTimeType() {
		return DepartureTimeType;
	}
	public void setDepartureTimeType(DepartureType departureTimeType) {
		DepartureTimeType = departureTimeType;
	}
	public PackageType getHolidayPackageType() {
		return HolidayPackageType;
	}
	public void setHolidayPackageType(PackageType holidayPackageType) {
		HolidayPackageType = holidayPackageType;
	}
	public String getDepartureDestination() {
		return DepartureDestination;
	}
	public void setDepartureDestination(String departureDestination) {
		DepartureDestination = departureDestination;
	}
	public String getArrivalDestination() {
		return ArrivalDestination;
	}
	public void setArrivalDestination(String arrivalDestination) {
		ArrivalDestination = arrivalDestination;
	}
	public String getDepartureDate() {
		return DepartureDate;
	}
	public void setDepartureDate(String departureDate) {
		DepartureDate = departureDate;
	}
	public String getArrivalDate() {
		return ArrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		ArrivalDate = arrivalDate;
	}
	public String getDepartureTime() {
		return DepartureTime;
	}
	public void setDepartureTime(String departureTime) {
		DepartureTime = departureTime;
	}
	public String getArrivalTime() {
		return ArrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		ArrivalTime = arrivalTime;
	}
	public String getPromoCode() {
		return PromoCode;
	}
	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}
	public String getHotelInOtherCity() {
		return HotelInOtherCity;
	}
	public void setHotelInOtherCity(String hotelInOtherCity) {
		HotelInOtherCity = hotelInOtherCity;
	}
}
