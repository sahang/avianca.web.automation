package auto.avianca.holidays.web.pojo;

import java.util.ArrayList;



public class ExpectedHolidayBookingEngine {
	
	private boolean DropLanguage 						= false;
	private boolean DefaultSelectedPackage 				= false;
	private boolean isFlightPlusHotelSelectedByDefault 	= false;
	private boolean ButtonFlightHotel     				= false;
	private boolean ButtonFlightHotelCar 				= false;
	private boolean ButtonFlightHotelActivities 		= false;
	private boolean LabelFlightHotel 					= false;
	private boolean LabelFlightHotelCar 				= false;
	private boolean LabelFlightHotelActivities 			= false;
	private boolean LabelFrom						 	= false;
	private boolean TextFrom 							= false;
	private boolean LabelTo 							= false;
	private boolean TextTo 								= false;
	private boolean LinkAllCities 						= false;
	private boolean LinkDestinationCities 				= false;
	private boolean LabelDepartDate 					= false;
	private boolean DTPDepartDate 						= false;
	private boolean LabelDepartTime 					= false;	
	private boolean DropDepartTime 						= false;
	private boolean LabelReturnDate 					= false;
	private boolean DTPReturnDate 						= false;
	private boolean LabelReturnTime 					= false;
	private boolean DropReturnTime 						= false;
	private boolean LabelRoomCount 						= false;
	private boolean DropRoomCount 						= false;
	private boolean LabelAdultCount 					= false;
	private boolean LabelAdultAge 						= false;
	private boolean DropAdultCount 						= false;
	private boolean LabelChildCount 					= false;
	private boolean LabelChildAge 						= false;
	private boolean DropChildCount 						= false;
	private boolean LabelInfantCount 					= false;
	private boolean LabelInfantAge 						= false;
	private boolean DropInfantCount 					= false;
	private boolean LabelSeatClass 						= false;
	private boolean DropSeatClass 						= false;
	private boolean LabelPromotionCode 					= false;
	private boolean TextPromotionCode 					= false;
	private boolean CheckDirectFirst 					= false;
	private boolean LabelDirectFirst 					= false;
	private boolean CheckPartialStay 					= false;
	private boolean LabelPartialStay 					= false;
	private boolean CheckHotelOtherCity 				= false;
	private boolean LabelHotelOtherCity 				= false ;
	private boolean ButtonSearch 						= false;
	private boolean MaximumRoomsAvailable 				= false;


	private String FlightPlusHotelLabel               	= "N/A";
	private String FlightPlusHotelPlusCarLabel        	= "N/A";
	private String FlightPlusHotelPlusActivityLabel   	= "N/A";
	private String FromLabel                          	= "N/A";
	private String ToLabel                            	= "N/A";
	private String AllCitiesLinkLabel                 	= "N/A";
	private String AllDestinationLinkLabel            	= "N/A";
	private String DepartDateLabel 						= "N/A";
	private String DepartureTimeLabel 					= "N/A";
	private String ReturnDateLabel 						= "N/A";
	private String ReturnTimeLabel 						= "N/A";
	private String SelectedRoomsLabel 					= "N/A";
	private String NumberOfAdultsLabel 					= "N/A";
	private String AdultAgeLabel 						= "N/A";
	private String NumberOfChildrenLabel 				= "N/A";
	private String ChildAgeLabel 						= "N/A";
	private String NumberOfInfantsLabel 				= "N/A";
	private String InfantAgeLabel 						= "N/A";
	private String SeatClassLabel 						= "N/A";
	private String PromotionCodeLabel 					= "N/A";
	
	
	
	private String DefaultLanguage 						= "N/A";
	private ArrayList<String> LanguageOptionsList 		= null;
	private String DefaultDepartureDate        			= "N/A";
	private String DefaultDepartureTime        			= "N/A";
	private ArrayList<String> DepartureTimeOptionsList 	= null;
	private String DefaultReturnDate        			= "N/A";
	private String DefaultReturnTime        			= "N/A";
	private ArrayList<String> ReturnTimeOptionsList 	= null;
	private String RoomDefaultSelectedCount        		= "N/A";
	private ArrayList<String> RoomCountList 			= null;
	private String AdultDefaultSelectedCount        	= "N/A";
	private ArrayList<String> AdultCountList 			= null;
	private String ChildDefaultSelectedCount        	= "N/A";
	private ArrayList<String> ChildCountList 			= null;
	private String InfantDefaultSelectedCount        	= "N/A";
	private ArrayList<String> InfantCountList 			= null;
	private String DefaultSeatClass        				= "N/A";
	private ArrayList<String> SeatClassOptionsList 		= null;



	public boolean isFlightPlusHotelSelectedByDefault() {
		return isFlightPlusHotelSelectedByDefault;
	}
	public void setFlightPlusHotelSelectedByDefault(
			boolean isFlightPlusHotelSelectedByDefault) {
		this.isFlightPlusHotelSelectedByDefault = isFlightPlusHotelSelectedByDefault;
	}
	public boolean isDropLanguage() {
		return DropLanguage;
	}
	public void setDropLanguage(boolean dropLanguage) {
		DropLanguage = dropLanguage;
	}
	public boolean isDefaultSelectedPackage() {
		return DefaultSelectedPackage;
	}
	public void setDefaultSelectedPackage(boolean defaultSelectedPackage) {
		DefaultSelectedPackage = defaultSelectedPackage;
	}
	public boolean isButtonFlightHotel() {
		return ButtonFlightHotel;
	}
	public void setButtonFlightHotel(boolean buttonFlightHotel) {
		ButtonFlightHotel = buttonFlightHotel;
	}
	public boolean isButtonFlightHotelCar() {
		return ButtonFlightHotelCar;
	}
	public void setButtonFlightHotelCar(boolean buttonFlightHotelCar) {
		ButtonFlightHotelCar = buttonFlightHotelCar;
	}
	public boolean isButtonFlightHotelActivities() {
		return ButtonFlightHotelActivities;
	}
	public void setButtonFlightHotelActivities(boolean buttonFlightHotelActivities) {
		ButtonFlightHotelActivities = buttonFlightHotelActivities;
	}
	public boolean isLabelFlightHotel() {
		return LabelFlightHotel;
	}
	public void setLabelFlightHotel(boolean labelFlightHotel) {
		LabelFlightHotel = labelFlightHotel;
	}
	public boolean isLabelFlightHotelCar() {
		return LabelFlightHotelCar;
	}
	public void setLabelFlightHotelCar(boolean labelFlightHotelCar) {
		LabelFlightHotelCar = labelFlightHotelCar;
	}
	public boolean isLabelFlightHotelActivities() {
		return LabelFlightHotelActivities;
	}
	public void setLabelFlightHotelActivities(boolean labelFlightHotelActivities) {
		LabelFlightHotelActivities = labelFlightHotelActivities;
	}
	public boolean isLabelFrom() {
		return LabelFrom;
	}
	public void setLabelFrom(boolean labelFrom) {
		LabelFrom = labelFrom;
	}
	public boolean isTextFrom() {
		return TextFrom;
	}
	public void setTextFrom(boolean textFrom) {
		TextFrom = textFrom;
	}
	public boolean isLabelTo() {
		return LabelTo;
	}
	public void setLabelTo(boolean labelTo) {
		LabelTo = labelTo;
	}
	public boolean isTextTo() {
		return TextTo;
	}
	public void setTextTo(boolean textTo) {
		TextTo = textTo;
	}
	public boolean isLinkAllCities() {
		return LinkAllCities;
	}
	public void setLinkAllCities(boolean linkAllCities) {
		LinkAllCities = linkAllCities;
	}
	public boolean isLinkDestinationCities() {
		return LinkDestinationCities;
	}
	public void setLinkDestinationCities(boolean linkDestinationCities) {
		LinkDestinationCities = linkDestinationCities;
	}
	public boolean isLabelDepartDate() {
		return LabelDepartDate;
	}
	public void setLabelDepartDate(boolean labelDepartDate) {
		LabelDepartDate = labelDepartDate;
	}
	public boolean isDTPDepartDate() {
		return DTPDepartDate;
	}
	public void setDTPDepartDate(boolean dTPDepartDate) {
		DTPDepartDate = dTPDepartDate;
	}
	public boolean isLabelDepartTime() {
		return LabelDepartTime;
	}
	public void setLabelDepartTime(boolean labelDepartTime) {
		LabelDepartTime = labelDepartTime;
	}
	public boolean isDropDepartTime() {
		return DropDepartTime;
	}
	public void setDropDepartTime(boolean dropDepartTime) {
		DropDepartTime = dropDepartTime;
	}
	public boolean isLabelReturnDate() {
		return LabelReturnDate;
	}
	public void setLabelReturnDate(boolean labelReturnDate) {
		LabelReturnDate = labelReturnDate;
	}
	public boolean isDTPReturnDate() {
		return DTPReturnDate;
	}
	public void setDTPReturnDate(boolean dTPReturnDate) {
		DTPReturnDate = dTPReturnDate;
	}
	public boolean isLabelReturnTime() {
		return LabelReturnTime;
	}
	public void setLabelReturnTime(boolean labelReturnTime) {
		LabelReturnTime = labelReturnTime;
	}
	public boolean isDropReturnTime() {
		return DropReturnTime;
	}
	public void setDropReturnTime(boolean dropReturnTime) {
		DropReturnTime = dropReturnTime;
	}
	public boolean isLabelRoomCount() {
		return LabelRoomCount;
	}
	public void setLabelRoomCount(boolean labelRoomCount) {
		LabelRoomCount = labelRoomCount;
	}
	public boolean isDropRoomCount() {
		return DropRoomCount;
	}
	public void setDropRoomCount(boolean dropRoomCount) {
		DropRoomCount = dropRoomCount;
	}
	public boolean isLabelAdultCount() {
		return LabelAdultCount;
	}
	public void setLabelAdultCount(boolean labelAdultCount) {
		LabelAdultCount = labelAdultCount;
	}
	public boolean isLabelAdultAge() {
		return LabelAdultAge;
	}
	public void setLabelAdultAge(boolean labelAdultAge) {
		LabelAdultAge = labelAdultAge;
	}
	public boolean isDropAdultCount() {
		return DropAdultCount;
	}
	public void setDropAdultCount(boolean dropAdultCount) {
		DropAdultCount = dropAdultCount;
	}
	public boolean isLabelChildCount() {
		return LabelChildCount;
	}
	public void setLabelChildCount(boolean labelChildCount) {
		LabelChildCount = labelChildCount;
	}
	public boolean isLabelChildAge() {
		return LabelChildAge;
	}
	public void setLabelChildAge(boolean labelChildAge) {
		LabelChildAge = labelChildAge;
	}
	public boolean isDropChildCount() {
		return DropChildCount;
	}
	public void setDropChildCount(boolean dropChildCount) {
		DropChildCount = dropChildCount;
	}
	public boolean isLabelInfantCount() {
		return LabelInfantCount;
	}
	public void setLabelInfantCount(boolean labelInfantCount) {
		LabelInfantCount = labelInfantCount;
	}
	public boolean isLabelInfantAge() {
		return LabelInfantAge;
	}
	public void setLabelInfantAge(boolean labelInfantAge) {
		LabelInfantAge = labelInfantAge;
	}
	public boolean isDropInfantCount() {
		return DropInfantCount;
	}
	public void setDropInfantCount(boolean dropInfantCount) {
		DropInfantCount = dropInfantCount;
	}
	public boolean isLabelSeatClass() {
		return LabelSeatClass;
	}
	public void setLabelSeatClass(boolean labelSeatClass) {
		LabelSeatClass = labelSeatClass;
	}
	public boolean isDropSeatClass() {
		return DropSeatClass;
	}
	public void setDropSeatClass(boolean dropSeatClass) {
		DropSeatClass = dropSeatClass;
	}
	public boolean isLabelPromotionCode() {
		return LabelPromotionCode;
	}
	public void setLabelPromotionCode(boolean labelPromotionCode) {
		LabelPromotionCode = labelPromotionCode;
	}
	public boolean isTextPromotionCode() {
		return TextPromotionCode;
	}
	public void setTextPromotionCode(boolean textPromotionCode) {
		TextPromotionCode = textPromotionCode;
	}
	public boolean isCheckDirectFirst() {
		return CheckDirectFirst;
	}
	public void setCheckDirectFirst(boolean checkDirectFirst) {
		CheckDirectFirst = checkDirectFirst;
	}
	public boolean isLabelDirectFirst() {
		return LabelDirectFirst;
	}
	public void setLabelDirectFirst(boolean labelDirectFirst) {
		LabelDirectFirst = labelDirectFirst;
	}
	public boolean isCheckPartialStay() {
		return CheckPartialStay;
	}
	public void setCheckPartialStay(boolean checkPartialStay) {
		CheckPartialStay = checkPartialStay;
	}
	public boolean isLabelPartialStay() {
		return LabelPartialStay;
	}
	public void setLabelPartialStay(boolean labelPartialStay) {
		LabelPartialStay = labelPartialStay;
	}
	public boolean isCheckHotelOtherCity() {
		return CheckHotelOtherCity;
	}
	public void setCheckHotelOtherCity(boolean checkHotelOtherCity) {
		CheckHotelOtherCity = checkHotelOtherCity;
	}
	public boolean isLabelHotelOtherCity() {
		return LabelHotelOtherCity;
	}
	public void setLabelHotelOtherCity(boolean labelHotelOtherCity) {
		LabelHotelOtherCity = labelHotelOtherCity;
	}
	public boolean isButtonSearch() {
		return ButtonSearch;
	}
	public void setButtonSearch(boolean buttonSearch) {
		ButtonSearch = buttonSearch;
	}
	public boolean isMaximumRoomsAvailable() {
		return MaximumRoomsAvailable;
	}
	public void setMaximumRoomsAvailable(boolean maximumRoomsAvailable) {
		MaximumRoomsAvailable = maximumRoomsAvailable;
	}
	public String getFlightPlusHotelLabel() {
		return FlightPlusHotelLabel;
	}
	public void setFlightPlusHotelLabel(String flightPlusHotelLabel) {
		FlightPlusHotelLabel = flightPlusHotelLabel;
	}
	public String getFlightPlusHotelPlusCarLabel() {
		return FlightPlusHotelPlusCarLabel;
	}
	public void setFlightPlusHotelPlusCarLabel(String flightPlusHotelPlusCarLabel) {
		FlightPlusHotelPlusCarLabel = flightPlusHotelPlusCarLabel;
	}
	public String getFlightPlusHotelPlusActivityLabel() {
		return FlightPlusHotelPlusActivityLabel;
	}
	public void setFlightPlusHotelPlusActivityLabel(
			String flightPlusHotelPlusActivityLabel) {
		FlightPlusHotelPlusActivityLabel = flightPlusHotelPlusActivityLabel;
	}
	public String getFromLabel() {
		return FromLabel;
	}
	public void setFromLabel(String fromLabel) {
		FromLabel = fromLabel;
	}
	public String getToLabel() {
		return ToLabel;
	}
	public void setToLabel(String toLabel) {
		ToLabel = toLabel;
	}
	public String getAllCitiesLinkLabel() {
		return AllCitiesLinkLabel;
	}
	public void setAllCitiesLinkLabel(String allCitiesLinkLabel) {
		AllCitiesLinkLabel = allCitiesLinkLabel;
	}
	public String getAllDestinationLinkLabel() {
		return AllDestinationLinkLabel;
	}
	public void setAllDestinationLinkLabel(String allDestinationLinkLabel) {
		AllDestinationLinkLabel = allDestinationLinkLabel;
	}
	public String getDepartDateLabel() {
		return DepartDateLabel;
	}
	public void setDepartDateLabel(String departDateLabel) {
		DepartDateLabel = departDateLabel;
	}
	public String getDefaultDepartureDate() {
		return DefaultDepartureDate;
	}
	public void setDefaultDepartureDate(String defaultDepartureDate) {
		DefaultDepartureDate = defaultDepartureDate;
	}
	public String getDefaultDepartureTime() {
		return DefaultDepartureTime;
	}
	public void setDefaultDepartureTime(String defaultDepartureTime) {
		DefaultDepartureTime = defaultDepartureTime;
	}
	public ArrayList<String> getDepartureTimeOptionsList() {
		return DepartureTimeOptionsList;
	}
	public void setDepartureTimeOptionsList(
			ArrayList<String> departureTimeOptionsList) {
		DepartureTimeOptionsList = departureTimeOptionsList;
	}
	public String getRoomDefaultSelectedCount() {
		return RoomDefaultSelectedCount;
	}
	public void setRoomDefaultSelectedCount(String roomDefaultSelectedCount) {
		RoomDefaultSelectedCount = roomDefaultSelectedCount;
	}
	
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public String getDepartureTimeLabel() {
		return DepartureTimeLabel;
	}
	public void setDepartureTimeLabel(String departureTimeLabel) {
		DepartureTimeLabel = departureTimeLabel;
	}
	public String getReturnDateLabel() {
		return ReturnDateLabel;
	}
	public void setReturnDateLabel(String returnDateLabel) {
		ReturnDateLabel = returnDateLabel;
	}
	public String getReturnTimeLabel() {
		return ReturnTimeLabel;
	}
	public void setReturnTimeLabel(String returnTimeLabel) {
		ReturnTimeLabel = returnTimeLabel;
	}
	public String getSelectedRoomsLabel() {
		return SelectedRoomsLabel;
	}
	public void setSelectedRoomsLabel(String selectedRoomsLabel) {
		SelectedRoomsLabel = selectedRoomsLabel;
	}
	public String getNumberOfAdultsLabel() {
		return NumberOfAdultsLabel;
	}
	public void setNumberOfAdultsLabel(String numberOfAdultsLabel) {
		NumberOfAdultsLabel = numberOfAdultsLabel;
	}
	public String getAdultAgeLabel() {
		return AdultAgeLabel;
	}
	public void setAdultAgeLabel(String adultAgeLabel) {
		AdultAgeLabel = adultAgeLabel;
	}
	public String getNumberOfChildrenLabel() {
		return NumberOfChildrenLabel;
	}
	public void setNumberOfChildrenLabel(String numberOfChildrenLabel) {
		NumberOfChildrenLabel = numberOfChildrenLabel;
	}
	public String getChildAgeLabel() {
		return ChildAgeLabel;
	}
	public void setChildAgeLabel(String childAgeLabel) {
		ChildAgeLabel = childAgeLabel;
	}
	public String getNumberOfInfantsLabel() {
		return NumberOfInfantsLabel;
	}
	public void setNumberOfInfantsLabel(String numberOfInfantsLabel) {
		NumberOfInfantsLabel = numberOfInfantsLabel;
	}
	public String getInfantAgeLabel() {
		return InfantAgeLabel;
	}
	public void setInfantAgeLabel(String infantAgeLabel) {
		InfantAgeLabel = infantAgeLabel;
	}
	public String getSeatClassLabel() {
		return SeatClassLabel;
	}
	public void setSeatClassLabel(String seatClassLabel) {
		SeatClassLabel = seatClassLabel;
	}
	public String getPromotionCodeLabel() {
		return PromotionCodeLabel;
	}
	public void setPromotionCodeLabel(String promotionCodeLabel) {
		PromotionCodeLabel = promotionCodeLabel;
	}
	public String getDefaultLanguage() {
		return DefaultLanguage;
	}
	public void setDefaultLanguage(String defaultLanguage) {
		DefaultLanguage = defaultLanguage;
	}
	public ArrayList<String> getLanguageOptionsList() {
		return LanguageOptionsList;
	}
	public void setLanguageOptionsList(ArrayList<String> languageOptionsList) {
		LanguageOptionsList = languageOptionsList;
	}
	public String getDefaultReturnDate() {
		return DefaultReturnDate;
	}
	public void setDefaultReturnDate(String defaultReturnDate) {
		DefaultReturnDate = defaultReturnDate;
	}
	public String getDefaultReturnTime() {
		return DefaultReturnTime;
	}
	public void setDefaultReturnTime(String defaultReturnTime) {
		DefaultReturnTime = defaultReturnTime;
	}
	public ArrayList<String> getReturnTimeOptionsList() {
		return ReturnTimeOptionsList;
	}
	public void setReturnTimeOptionsList(ArrayList<String> returnTimeOptionsList) {
		ReturnTimeOptionsList = returnTimeOptionsList;
	}
	public ArrayList<String> getRoomCountList() {
		return RoomCountList;
	}
	public void setRoomCountList(ArrayList<String> roomCountList) {
		RoomCountList = roomCountList;
	}
	public String getAdultDefaultSelectedCount() {
		return AdultDefaultSelectedCount;
	}
	public void setAdultDefaultSelectedCount(String adultDefaultSelectedCount) {
		AdultDefaultSelectedCount = adultDefaultSelectedCount;
	}
	public ArrayList<String> getAdultCountList() {
		return AdultCountList;
	}
	public void setAdultCountList(ArrayList<String> adultCountList) {
		AdultCountList = adultCountList;
	}
	public String getChildDefaultSelectedCount() {
		return ChildDefaultSelectedCount;
	}
	public void setChildDefaultSelectedCount(String childDefaultSelectedCount) {
		ChildDefaultSelectedCount = childDefaultSelectedCount;
	}
	public ArrayList<String> getChildCountList() {
		return ChildCountList;
	}
	public void setChildCountList(ArrayList<String> childCountList) {
		ChildCountList = childCountList;
	}
	public String getInfantDefaultSelectedCount() {
		return InfantDefaultSelectedCount;
	}
	public void setInfantDefaultSelectedCount(String infantDefaultSelectedCount) {
		InfantDefaultSelectedCount = infantDefaultSelectedCount;
	}
	public ArrayList<String> getInfantCountList() {
		return InfantCountList;
	}
	public void setInfantCountList(ArrayList<String> infantCountList) {
		InfantCountList = infantCountList;
	}
	public String getDefaultSeatClass() {
		return DefaultSeatClass;
	}
	public void setDefaultSeatClass(String defaultSeatClass) {
		DefaultSeatClass = defaultSeatClass;
	}
	public ArrayList<String> getSeatClassOptionsList() {
		return SeatClassOptionsList;
	}
	public void setSeatClassOptionsList(ArrayList<String> seatClassOptionsList) {
		SeatClassOptionsList = seatClassOptionsList;
	}

}
