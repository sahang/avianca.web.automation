package auto.avianca.holidays.web.pojo;

public class CarSearchScenario {
	
	private String PickupLocation = "";
	private String ReturnLocation = "";
	
	public String getPickupLocation() {
		return PickupLocation;
	}
	public void setPickupLocation(String pickupLocation) {
		PickupLocation = pickupLocation;
	}
	public String getReturnLocation() {
		return ReturnLocation;
	}
	public void setReturnLocation(String returnLocation) {
		ReturnLocation = returnLocation;
	}
	
	

}
