package auto.avianca.holidays.web.pojo;

public class HotelSearchScenario {

	private String ToLocation 	= "";
	private String CheckInDate 	= "";
	private String CheckOutDate = "";
	private String HotelName 	= "";
	
	public String getToLocation() {
		return ToLocation;
	}
	public void setToLocation(String toLocation) {
		ToLocation = toLocation;
	}
	public String getCheckInDate() {
		return CheckInDate;
	}
	public void setCheckInDate(String checkInDate) {
		CheckInDate = checkInDate;
	}
	public String getCheckOutDate() {
		return CheckOutDate;
	}
	public void setCheckOutDate(String checkOutDate) {
		CheckOutDate = checkOutDate;
	}
	public String getHotelName() {
		return HotelName;
	}
	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}
}
